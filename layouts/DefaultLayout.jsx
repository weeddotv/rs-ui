import React, { useEffect, useState } from "react";
import { Footer, Header, Seo } from "../components";
import styled from "styled-components";

const LayoutStyled = styled.main``;

const DefaultLayout = ({ seo, children }) => {
  return (
    <LayoutStyled>
      <Seo
        title={seo?.metaTitle}
        description={seo?.metaDescription}
        keywords={seo?.metaKeywords}
        image={seo?.ogImage}
      />

      <Header />

      {children}
      <Footer />
    </LayoutStyled>
  );
};

export async function getServerSideProps(context) {
  return {
    props: {},
  };
}

export default DefaultLayout;
