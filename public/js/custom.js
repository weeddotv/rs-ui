$(document).ready(function () {
  init();

  function init() {
    $(".hamb").click(function () {
      var clicks = $(this).data("clicks");
      if (clicks) {
        $("body").css("overflow", "auto");
      } else {
        $("body").css("overflow", "hidden");
      }
      $(this).data("clicks", !clicks);
    });
    $(".navBack").click(function () {
      $(".hamb").click();
    });

    !(function (o, c) {
      var n = c.documentElement,
        t = " w-mod-";
      (n.className += t + "js"),
        ("ontouchstart" in o ||
          (o.DocumentTouch && c instanceof DocumentTouch)) &&
          (n.className += t + "touch");
    })(window, document);
  }


});
