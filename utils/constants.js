export const BLOCK_INFO = [
  {
    id: 1,
    imageURL: "/images/Hand-RSVG.svg",
    imageAlt: "Digital ownership icon",
    title: "Digital ownership",
    description:
      "NFT&#x27;s allow you to own one-of-a-kind digital goods. Your purchases can be verified as originating from Rolling Stone making it impossible to fake.",
  },
  {
    id: 2,
    imageURL: "/images/Asset-RSVG.svg",
    imageAlt: "Value in scarcity icon",
    title: "Value in scarcity",
    description:
      "There are  a limited number of NFT&#x27;s for any given asset - photo, video, music. This gives your NFT rarity and value to collectors as a painting would.",
  },
  {
    id: 3,
    imageURL: "/images/Carbon-RSVG.svg",
    imageAlt: "Carbon Neutral",
    title: "Carbon Neutral",
    description:
      "Climate Care awarded our NFT technology a Carbon Neutral Certification in early 2021.",
  },
  {
    id: 4,
    imageURL: "/images/Wax-RSVG.svg",
    imageAlt: "WAX logo icon",
    title: "WAX technology",
    description:
      "Your Rolling Stone NFT&#x27;s built on WAX, one of the worlds most used NFT ecosystems used by 325k daily users for NTF&#x27;s on everything from Major League Baseball to Street Fighter..",
  },
];

export const AUTH_TOKEN = "auth-token";
