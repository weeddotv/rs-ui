import React from "react";
import { formatText } from "../../utils/global";
import Countdown from "react-countdown";

const BlockCollection = ({
  className = "",
  imageURL = "",
  title = "",
  subTitle = "",
  description = "",
  showPack = false,
  countDown = {
    show: false,
    time: null,
  },
  packs = [],
}) => {
  return (
    <div
      className={`collection-feature is--past ${className}`}
      data-w-id="acfc6bb2-7ed9-df0a-a12c-7e4bd516d097"
    >
      <div className="collection-left">
        <div
          className="image-mag"
          data-w-id="56588899-b242-f019-cf9e-d3b35bc3c97c"
        >
          <div
            className="div-block"
            style={{ backgroundImage: `url(${imageURL})` }}
          ></div>
          <img
            src="/images/RedFrameSVG.svg"
            loading="lazy"
            alt={title}
            className="magazine-red-back"
          />
        </div>
      </div>
      <div className="collection-right is--past">
        <div className="status-collection-div">
          <div className="status-live is--centre">
            <div className="heading-3">
              {subTitle}{" "}
              {countDown.show ? (
                Date.now() - new Date(countDown?.time).valueOf() >= 0 ? (
                  "live now"
                ) : (
                  <Countdown date={new Date(countDown?.time).valueOf()} />
                )
              ) : null}
            </div>
          </div>
        </div>
        <h1 className="heading-8">{title}</h1>
        <div className="collection-description w-richtext">
          {formatText(description)}
        </div>
        {showPack ? (
          <div className="collection-list-wrapper-4 w-dyn-list">
            <div role="list" className="w-dyn-items">
              {packs?.map((item) => (
                <div
                  role="listitem"
                  className="collection-item-9 w-dyn-item"
                  key={item?._id}
                >
                  <a href={`/pack/${item?._id}`} className="link">
                    {item?.name}
                  </a>
                </div>
              ))}
            </div>
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default BlockCollection;
