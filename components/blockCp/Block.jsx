import React from "react";

const Block = ({
  className = "",
  imageURL = "",
  imageAlt = "",
  title = "",
  description = "",
}) => {
  return (
    <div className={`benefit-list ${className}`}>
      <div className="perspective">
        <div className="cube2">
          <div className="c1"></div>
          <div className="c2"></div>
          <div className="c3">
            <img
              src={imageURL}
              loading="lazy"
              alt={imageAlt}
              className="image-12"
            />
          </div>
          <div className="c4">
            <img
              src={imageURL}
              loading="lazy"
              alt={imageAlt}
              className="image-12"
            />
          </div>
          <div className="c5"></div>
          <div className="c6"></div>
        </div>
      </div>
      <div className="font-contain">
        <div className="benefit-heading">{title}</div>
        <p
          className="is--blackfont"
          dangerouslySetInnerHTML={{ __html: description }}
        ></p>
      </div>
    </div>
  );
};

export default Block;
