import React from "react";
import Link from "next/link";

const Footer = () => {
  return (
    <div className="footer">
      <div className="container is--footer">
        <div className="footer-contents">
          <div className="footer-content-items">
            <div className="heading-footer">Rolling Stone</div>
            <a
              href="https://www.youtube.com/channel/UC5ogXwEsy_q8_2DQHp1RU8A"
              className="footer-links"
              target="_blank"
              rel="noreferrer"
            >
              Youtube
            </a>
            <a
              href="https://www.facebook.com/rollingstoneaustralia/"
              target="_blank"
              className="footer-links"
              rel="noreferrer"
            >
              Facebook
            </a>
            <a
              href="https://www.instagram.com/rollingstoneaus/"
              target="_blank"
              className="footer-links"
              rel="noreferrer"
            >
              Instagram
            </a>
            <a
              href="https://twitter.com/rollingstoneaus"
              target="_blank"
              className="footer-links"
              rel="noreferrer"
            >
              Twitter
            </a>
            <a
              href="https://au.rollingstone.com"
              className="footer-links"
              target="_blank"
              rel="noreferrer"
            >
              Website
            </a>
          </div>
          <div className="footer-content-items">
            <div className="heading-footer">The Brag</div>
            <a
              href="https://www.youtube.com/channel/UCcZMmtU74qKN_w4Dd8ZkV6g"
              target="_blank"
              className="footer-links"
              rel="noreferrer"
            >
              Youtube
            </a>
            <a
              href="https://www.facebook.com/thebragmag"
              target="_blank"
              className="footer-links"
              rel="noreferrer"
            >
              Facebook
            </a>
            <a
              href="https://www.instagram.com/thebragmag/"
              className="footer-links"
              target="_blank"
              rel="noreferrer"
            >
              Instagram
            </a>
            <a
              href="https://twitter.com/TheBrag"
              className="footer-links"
              target="_blank"
              rel="noreferrer"
            >
              Twitter
            </a>
            <a
              href="https://thebrag.com"
              target="_blank"
              className="footer-links"
              rel="noreferrer"
            >
              Website
            </a>
          </div>
          <div className="footer-content-items">
            <div className="heading-footer">About</div>
            <Link href="/faq">
              <a className="footer-links">FAQ</a>
            </Link>
            <Link href="/privacy">
              <a className="footer-links">Privacy</a>
            </Link>
            <Link href="/terms">
              <a className="footer-links">Terms</a>
            </Link>
            <Link href="/about-us">
              <a className="footer-links">About us</a>
            </Link>
          </div>
        </div>
        <div className="footer-logo">
          <img
            src="/images/RSBadgePNG.png"
            loading="lazy"
            alt="Rolling Stone Australia logo badge"
            className="image-4"
          />
          <img
            src="/images/TheBragPNG.png"
            loading="lazy"
            alt="The Brag logo"
            className="image-5"
          />
          <div className="text-block-4">
            <a href="https://scarce.studio/">
              <span className="text-span-2">Powered by: Scarce.Studio</span>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Footer;
