import React from "react";
import { formatListText } from "../../utils/global";
import Button from "../buttonCp/Button";

const PackItemDetail = ({
  imageURL,
  className = "",
  productId,
  name,
  price,
  amount,
  remainAmountText,
  description,
  soldOut = false,
  handleOnClick,
}) => {
  return (
    <div className={`container is--hero ${className}`}>
      <div className="product-left">
        <img src={imageURL} loading="lazy" alt="" className="image-6" />
      </div>
      <div className="product-right">
        <div className="grid-em1-below">
          <div className="w-dyn-list">
            <div role="list" className="w-dyn-items">
              <div role="listitem" className="w-dyn-item">
                <h1 className="h1-product">{name}</h1>
              </div>
            </div>
          </div>
        </div>
        <div className="grid-em2-below">
          <div className="item-bold1 is--left is--space">{amount} NFT's</div>
          <div className="item-bold1 is--left is--space">${price}</div>
        </div>
        <div
          className="rich-text-block w-richtext"
          style={{ marginBottom: "30px" }}
        >
          {formatListText(description)}
        </div>

        {!soldOut ? (
          <Button
            handleOnClick={(e) => {
              e.preventDefault();
              handleOnClick(productId);
            }}
          >
            GET THIS PACK
          </Button>
        ) : null}

        <div className="item-status-table is--left is--morespace">
          {soldOut ? "SOLD OUT" : remainAmountText}
        </div>

        {soldOut ? (
          <div className="secondary-market">
            <h3 className="heading-13">Missed this?</h3>
            <p className="paragraph-10">
              It might be for sale in a secondary market such as{" "}
              <a
                href="https://wax.atomichub.io/"
                target="_blank"
                rel="noreferrer"
              >
                AtomicHub.
              </a>
            </p>
            <img
              src="https://assets.website-files.com/611c8104e15cb659481fb48d/612b299e8914695e210a7ab1_atomic%20hub.svg"
              loading="lazy"
              alt="Atomic hub logo"
              className="image-9"
            />
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default PackItemDetail;
