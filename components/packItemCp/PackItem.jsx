import React from "react";
import Button from "../buttonCp/Button";

const PackItem = ({
  imageURL,
  className = "",
  packURL,
  price,
  amount,
  remainAmountText,
  description,
  soldOut = false,
}) => {
  return (
    <div
      role="listitem"
      className={`collection-item-3 w-dyn-item rol-pack-item ${className}`}
    >
      <div className="pack-header">
        <div className="product-image">
          <img src={imageURL} loading="lazy" alt="" />
        </div>
        <div className="div-block-18 is--desktop">
          <div className="item-bold1 is--main">${price}</div>
          <p className="item-nft-number">{amount} NFT's</p>
          <p className="item-status-table is--main">{remainAmountText}</p>
        </div>
      </div>
      <div className="div-block-19">
        <div className="item-list w-richtext">{description}</div>

        <Button className="is--collection" href={packURL} disabled={soldOut}>
          {!soldOut ? "GET PACK " : "SOLD OUT"}
        </Button>
      </div>
    </div>
  );
};

export default PackItem;
