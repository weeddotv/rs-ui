import React from "react";

const NFTItem = ({
  imageURL,
  className = "",
  score = 0,
  NFTName,
  amountText,
  description,
}) => {
  return (
    <div
      role="listitem"
      className={`collection-item-10 w-dyn-item ${className}`}
    >
      <div className="details">
        <div
          data-w-id="121225c5-a3f5-dc17-79c4-699242d48545"
          className="img-nft-div"
        >
          <img
            alt="NFT image for Rolling Stone"
            loading="lazy"
            src={imageURL}
            className="img-nft"
          />
        </div>
        <div className="inventory-heading">{NFTName}</div>
        {amountText ? (
          <div className="inventory-heading is--red">{amountText}</div>
        ) : null}
        {!!score && score > 0 ? (
          <div className="div-collectorscore">
            <div className="text-block-3">COLLECTORS SCORE: </div>
            <div className="text-block-3">{score}</div>
          </div>
        ) : null}
        <p className="paragraph">{description}</p>
      </div>
    </div>
  );
};

export default NFTItem;
