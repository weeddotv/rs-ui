import React from "react";
import Link from "next/link";

const LeaderBoardItem = ({
  className = "",
  imageURL = "/images/defaultAvatar.svg",
  email,
  instagramURL,
  twitterURL,
  score,
  rank,
  stashURL,
}) => {
  return (
    <div className={`w-layout-grid grid ${className}`}>
      <div
        id="w-node-_7f749d02-a5d3-a3c6-7c89-c38d47b10180-158eb018"
        className="grid-hold"
      >
        <img
          src={imageURL}
          loading="lazy"
          alt="Avatar for user account page"
          className="image-profile"
        />
      </div>
      <div className="grid-hold">
        <div className="name">
          <div className="leadertext">{email}</div>
        </div>
      </div>
      <div
        id="w-node-_7f749d02-a5d3-a3c6-7c89-c38d47b10186-158eb018"
        className="grid-hold is--right"
      >
        {instagramURL ? (
          <a
            href={instagramURL}
            className="link-block-4 is--lessspace w-inline-block"
            target="_blank"
            rel="noreferrer"
          >
            <img
              src="/images/Instagram.svg"
              loading="lazy"
              alt="Instagram icon"
              className="leader-icon"
            />
          </a>
        ) : null}
      </div>
      <div
        id="w-node-_7f749d02-a5d3-a3c6-7c89-c38d47b10189-158eb018"
        className="grid-hold"
      >
        {twitterURL ? (
          <a
            href={twitterURL}
            className="link-block-4 is--lessspace w-inline-block"
            target="_blank"
            rel="noreferrer"
          >
            <img
              src="/images/Twitter.svg"
              loading="lazy"
              alt="Twitter icon"
              className="leader-icon"
            />
          </a>
        ) : null}
      </div>
      <div
        id="w-node-_7f749d02-a5d3-a3c6-7c89-c38d47b1018c-158eb018"
        className="grid-hold"
      >
        <div className="div-hold-leader">
          <img
            src="/images/Collect.svg"
            loading="lazy"
            alt="Collectors score item"
            className="image-13"
          />
          <div className="leadertext">Score:</div>
          <div className="leadertext is--score">{score}</div>
        </div>
      </div>
      <div
        id="w-node-_7f749d02-a5d3-a3c6-7c89-c38d47b10193-158eb018"
        className="grid-hold"
      >
        <div className="div-hold-leader">
          <img
            src="/images/Medal.svg"
            loading="lazy"
            alt="Medal item"
            className="image-13"
          />
          <div className="leadertext">Rank:</div>
          <div className="leadertext is--score">{rank}</div>
        </div>
      </div>
      <div
        id="w-node-_7f749d02-a5d3-a3c6-7c89-c38d47b1019a-158eb018"
        className="grid-hold is--right"
      >
        <p className="paragraph-4">
          <Link href={stashURL}>
            <a data-w-id="7f749d02-a5d3-a3c6-7c89-c38d47b1019c">View Stash</a>
          </Link>
        </p>
      </div>
    </div>
  );
};

export default LeaderBoardItem;
