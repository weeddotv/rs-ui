import React from "react";
import Link from "next/link";

const Button = ({
  href = "#",
  className,
  dataWID,
  children,
  handleOnClick,
  disabled = false,
}) => {
  return (
    <div
      className={`main-left-button ${className} ${
        disabled ? "isDisabled" : ""
      }`}
    >
      <Link href={href}>
        <a
          data-w-id={dataWID || "3f6150e0-1760-3eda-c01d-1207fa829e75"}
          className="button-top w-inline-block"
          onClick={handleOnClick}
        >
          <div className="button-text">{children}</div>
        </a>
      </Link>

      <div className="button-back"></div>
    </div>
  );
};

export default Button;
