import React, { useEffect } from "react";
import Slider from "react-slick";
import styled from "styled-components";

const SlideStyled = styled.div`
  .rs-slide {
    width: 188px !important;
    height: 242px;
    img {
      width: 100%;
      height: 100%;
      object-fit: cover;
    }
  }
  @media (min-width: 1080px) {
    .rs-slide {
      width: 355px !important;
      height: 406px;
    }
  }
`;

const albums = [
  {
    src: "/images/1.jpg",
    alt: "One of Rolling Stone Australia&#x27;s magazine covers 1",
  },
  {
    src: "/images/2.jpg",
    alt: "One of Rolling Stone Australia&#x27;s magazine covers 2",
  },
  {
    src: "/images/4.jpg",
    alt: "One of Rolling Stone Australia&#x27;s magazine covers 3",
  },
  {
    src: "/images/10.jpg",
    alt: "One of Rolling Stone Australia&#x27;s magazine covers 4",
  },
  {
    src: "/images/11.jpg",
    alt: "One of Rolling Stone Australia&#x27;s magazine covers 5",
  },
  {
    src: "/images/8.jpg",
    alt: "One of Rolling Stone Australia&#x27;s magazine covers 6",
  },
  {
    src: "/images/9.jpg",
    alt: "One of Rolling Stone Australia&#x27;s magazine covers 7",
  },
  {
    src: "/images/7.jpg",
    alt: "One of Rolling Stone Australia&#x27;s magazine covers 8",
  },
];

const MagazineScroll = () => {
  const settings = {
    dots: false,
    infinite: true,
    variableWidth: true,
    slidesToScroll: 4,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 1,
          // variableWidth: false,
        },
      },
    ],
  };

  return (
    <SlideStyled>
      <Slider {...settings}>
        {albums?.map((item) => (
          <div className="rs-slide" key={item?.src}>
            <img src={item?.src} alt={item?.alt} />
          </div>
        ))}
      </Slider>
    </SlideStyled>
  );
};

export default MagazineScroll;
