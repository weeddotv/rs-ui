import React, { useEffect, useState } from "react";
import Link from "next/link";
import { Login, Logout } from "../googleLogin";
import { Desktop, NotDesktop } from "../../utils/mediaQuery";
import { useSelector } from "react-redux";
import { LoadingGlobal } from "../../components";

const Header = () => {
  const { user, loading } = useSelector((state) => ({
    user: state.authStore.user,
    loading: state.globalStore.loading,
  }));

  useEffect(() => {
    document.addEventListener("scroll", function (e) {
      const mobileCover = document.getElementById("mobile-cover");
      if (window.scrollY > 200) {
        if (!mobileCover.classList.contains("isActive")) {
          mobileCover.classList.add("isActive");
        }
      } else {
        if (mobileCover.classList.contains("isActive")) {
          mobileCover.classList.remove("isActive");
        }
      }
    });
  }, []);

  return (
    <>
      <LoadingGlobal className={loading ? "isActive" : ""} />
      <div className="nav-menu">
        <div className="mobile-cover" id="mobile-cover"></div>
        <div className="nav-hold">
          <div id="navBack" className="mobile-back"></div>
          <div className="mobile-menu">
            <div className="menu-contents">
              <div className="red-line"></div>

              <Link href="/#collection-feature">
                <a className="link-block-2 w-inline-block">
                  <div className="menu-text">PACKS</div>
                </a>
              </Link>
            </div>
            {/* <div className="menu-contents">
              <div className="red-line"></div>
              <Link href="/leaderboard">
                <a className="link-block-2 w-inline-block">
                  <div className="menu-text">LEADERBOARD</div>
                </a>
              </Link>
            </div>
            <div className="menu-contents">
              <div className="red-line"></div>
              <Link href="/past-collections">
                <a className="link-block-2 w-inline-block">
                  <div className="menu-text">ARCHIVE</div>
                </a>
              </Link>
            </div> */}

            {user ? (
              <>
                <div className="menu-contents">
                  <div className="red-line"></div>
                  <Link href={`/${user?._id}`}>
                    <a className="link-block-2 w-inline-block">
                      <div className="menu-text">STASH</div>
                    </a>
                  </Link>
                </div>
                <div className="menu-contents">
                  <div className="red-line"></div>
                  <Link href="/account">
                    <a className="link-block-2 w-inline-block">
                      <div className="menu-text">ACCOUNT</div>
                    </a>
                  </Link>
                </div>
                <div className="menu-contents">
                  <div className="red-line"></div>
                  <NotDesktop>
                    <Logout
                      className="link-block-2 w-inline-block"
                      text="LOGOUT"
                    />
                  </NotDesktop>
                </div>
              </>
            ) : (
              <div className="menu-contents">
                <div className="red-line"></div>
                <NotDesktop>
                  <Login
                    className="link-block-2 w-inline-block"
                    text="LOGIN/SIGN UP"
                  />
                </NotDesktop>
              </div>
              // !user && (
              //   <div className="menu-contents">
              //     <div className="red-line"></div>
              //     <Login className="link-block-2 w-inline-block" />
              //   </div>
              // )
            )}
          </div>
          <div className="desktop-menu">
            <Link href="/">
              <a
                aria-current="page"
                className="rs-logo-div w-inline-block w--current"
              >
                <img
                  src="/images/RollingStoneLogo.png"
                  loading="lazy"
                  alt="Rolling Stone Australia log"
                  className="rs-logo"
                />
              </a>
            </Link>

            <div className="link-nav-div">
              <a href="/#collection-feature" className="link is--darker">
                Packs
              </a>
              {/* <Link href="/leaderboard">
                <a className="link is--darker">Leaderboard</a>
              </Link>
              <Link href="/past-collections">
                <a className="link is--darker">Archive</a>
              </Link> */}
              {user ? (
                <>
                  <Link href={`/${user?._id}`}>
                    <a className="link is--darker">Stash</a>
                  </Link>
                  <Link href="/account">
                    <a className="link is--darker">Account</a>
                  </Link>
                  <Desktop>
                    <Logout className="link is--darker" />
                  </Desktop>
                </>
              ) : (
                <Desktop>
                  {/* {!user && <Login className="link is--darker" />} */}
                  <Login className="link is--darker" />
                </Desktop>
              )}
            </div>
          </div>
          <div
            data-w-id="e4f2d8b3-fcc5-7995-4996-ebe6ce1e5332"
            className="hamb is--account"
          >
            <div
              data-w-id="e4f2d8b3-fcc5-7995-4996-ebe6ce1e5333"
              data-animation-type="lottie"
              data-src="/documents/Hamburger-menu.json"
              data-loop="0"
              data-direction="1"
              data-autoplay="0"
              data-is-ix2-target="1"
              data-renderer="svg"
              data-default-duration="2"
              data-duration="0"
              data-ix2-initial-state="0"
              className="hamburger"
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
