import React from 'react';
import styled from 'styled-components';

const ImageStyled = styled.div`
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  width: 100%;
  height: 100%;
  background-image: url('${(props) => props.backgroundImage}');
`;

const Image = ({ src = '', className = '' }) => {
  return <ImageStyled backgroundImage={src} className={className} />;
};

export default Image;
