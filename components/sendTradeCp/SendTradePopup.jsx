import React from "react";
import { Form, Input, Button as ButtonAntd } from "antd";

const SendTradePopup = ({ onSend, sendTradeData, onClose }) => {
  const [form] = Form.useForm();

  return (
    <div className="send-trade-popup">
      <div className="container-popup">
        <button
          data-w-id="93e2d941-c374-c8b4-f45c-739de4a07fda"
          className="close-div"
          onClick={onClose}
        >
          <img
            src="images/CrossSVG.svg"
            loading="lazy"
            alt="Close down send and transfer popup"
          />
        </button>
        <h3 className="heading-10">Send / Trade</h3>
        <p>
          Send this NFT to your personal wallet. From here you can list this NFT
          for sale on marketplaces like {' '}
          <a href="https://wax.atomichub.io/" target="_blank" rel="noreferrer">
            AtomicHub
          </a>
        </p>
        <div className="get-alerted-form send-trade-form-custom">
          <Form
            form={form}
            onFinish={(values) =>
              onSend({ ...values, assetId: sendTradeData?.assetId })
            }
          >
            <Form.Item
              name="to"
              rules={[{ required: true, message: "Email is required" }]}
            >
              <Input placeholder="Enter WAX Wallet" maxLength="256" />
            </Form.Item>
            <Form.Item shouldUpdate>
              {() => (
                <ButtonAntd
                  className="ov-btn full-width"
                  htmlType="submit"
                  loading={sendTradeData?.loading}
                  disabled={
                    !!form
                      .getFieldsError()
                      .filter(({ errors }) => errors.length).length
                  }
                >
                  SUBMIT
                </ButtonAntd>
              )}
            </Form.Item>
          </Form>

          {sendTradeData?.loading === false &&
          sendTradeData?.success === true ? (
            <div className="success-message">
              <div className="error-text">
                Transfer successful!
              </div>
            </div>
          ) : null}
          {sendTradeData?.loading === false &&
          sendTradeData?.success === false ? (
            <div className="error-message-2 ">
              <div className="error-text">
                Hmmm, something went wrong. Please try again!
              </div>
            </div>
          ) : null}
        </div>
        <p>
          Don&#x27;t have an account?
          <br />‍
          <a href="https://all-access.wax.io/" target="_blank" rel="noreferrer">
            Create a FREE WAX Cloud Wallet in 2 clicks!
          </a>
        </p>
      </div>
    </div>
  );
};

export default SendTradePopup;
