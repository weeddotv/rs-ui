import React from "react";
import Link from "next/link";
import { useDispatch } from "../../hooks";
import Router from "next/router";

const StashItem = ({
  imageURL,
  className,
  stashURL,
  score,
  stashName,
  remainAmountText,
  description,
  assetId,
  sendStradeOnclick = () => {},
  openURL,
  redeemURL,
  packable,
  redeemAble = false,
  isOwner = false,
}) => {
  const { setOpenPackSelected } = useDispatch((state) => ({
    setOpenPackSelected: state.packStore.setOpenPackSelected,
  }));
  const onOpenPack = () => {
    setOpenPackSelected({ imageURL: imageURL });
    Router.push(openURL);
  };
  return (
    <div
      data-w-id="639af8a6-1f71-705f-6c24-c287b3390705"
      role="listitem"
      className={`collection-item-10 w-dyn-item ${className}`}
    >
      <Link href={stashURL}>
        <a
          data-w-id="b029e8be-6540-f693-4b0b-041c6525208f"
          className="img-nft-div pack w-inline-block"
        >
          <img
            src={imageURL}
            loading="lazy"
            alt="stash image"
            className="img-nft"
          />
        </a>
      </Link>

      <div className="details">
        <div className="inventory-heading">{stashName}</div>
        <div className="inventory-heading is--red">{remainAmountText}</div>
        <div className="div-collectorscore">
          <div className="text-block-3">COLLECTORS SCORE: </div>
          <div className="text-block-3">{score}</div>
        </div>
        <p className="paragraph">{description}</p>
        {isOwner ? (
          <div className="options-div">
            {packable ? (
              <button className="link" onClick={onOpenPack}>
                Open
              </button>
            ) : (
              <>
                {redeemAble ? (
                  <Link href={redeemURL}>
                    <a className="link">Redeem</a>
                  </Link>
                ) : null}

                <button
                  data-w-id="96305fef-1938-ccb5-827d-f3c95ffac281"
                  className="link"
                  onClick={() => sendStradeOnclick(assetId)}
                >
                  Send/Trade
                </button>
              </>
            )}
          </div>
        ) : null}
      </div>
    </div>
  );
};

export default StashItem;
