import Head from "next/head";

const Seo = (props) => {
  return (
    <Head>
      <title>{props?.title}</title>
      <meta name="title" content={props?.title} />
      <meta name="referrer" content="always" />
      <meta property="description" content={props.description} />
      <meta
        property="og:url"
        content={props.url}
      />
      <meta
        property="og:description"
        content={props.description}
        key="ogdesc"
      />
      <meta property="og:image" content={props.image} key="ogimage" />
      <meta name="keywords" content={props.keywords} />

    </Head>
  );
};

export default Seo;
