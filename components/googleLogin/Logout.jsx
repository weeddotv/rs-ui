import React, { useState, useEffect } from "react";

import { useRouter } from "next/router";
import { GoogleLogout } from "react-google-login";
import { useDispatch } from "../../hooks";
import { AuthService, UserService } from "../../services";

const clientId = process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID;

function Logout({ className = "", text = "Logout" }) {
  const [mounted, setMounted] = useState(false);

  const Router = useRouter();
  const { setUser, setGoogleAuth } = useDispatch((state) => ({
    setUser: state.authStore.setUser,
    setGoogleAuth: state.authStore.setGoogleAuth,
  }));
  const onSuccess = () => {
    setUser(null);
    setGoogleAuth(null);
    AuthService.signOut();
    UserService.updateTokenId(null);

    if (window?.gapi) {
      const auth2 = window?.gapi?.auth2.getAuthInstance();
      if (auth2 != null) {
        auth2.signOut().then(() => {
          auth2.disconnect().then(console.log("LOGOUT SUCCESSFUL"));
        });
      }
    }

    Router.push("/");
  };

  useEffect(() => {
    setMounted(true);
  }, []);
  return (
    mounted && (
      <GoogleLogout
        clientId={clientId}
        buttonText={text}
        onLogoutSuccess={onSuccess}
        className={`rol-btn-auth ${className}`}
      />
    )
  );
}

export default Logout;
