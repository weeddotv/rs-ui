import React from "react";
import { useGoogleLogout } from "react-google-login";

const clientId = process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID;

function LogoutHooks() {
  const onLogoutSuccess = (res) => {
    console.log("Logged out Success");
  };

  const onFailure = () => {
    console.log("Handle failure cases");
  };

  const { signOut } = useGoogleLogout({
    clientId,
    onLogoutSuccess,
    onFailure,
  });

  return (
    <button onClick={signOut} className="button">
      <img src="icons/google.svg" alt="google login" className="icon"></img>

      <span className="buttonText">Sign out</span>
    </button>
  );
}

export default LogoutHooks;
