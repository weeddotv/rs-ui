import React, { useState, useEffect } from "react";

import { GoogleLogin } from "react-google-login";
// refresh token
import { refreshTokenSetup } from "../../utils/refreshToken";
import { AuthService, UserService } from "../../services";
import { useDispatch } from "../../hooks";

const clientId = process.env.NEXT_PUBLIC_GOOGLE_CLIENT_ID;

function Login({ className = "", text = "Login/Sign up" }) {
  const [mounted, setMounted] = useState(false);
  const { getMe, setGoogleAuth } = useDispatch((state) => ({
    getMe: state.authStore.getMe,
    setGoogleAuth: state.authStore.setGoogleAuth,
  }));

  const onSuccess = (res) => {
    AuthService.setToken(res?.tokenId);
    UserService.updateTokenId(res?.tokenId);
    getMe();
    setGoogleAuth(res);
    refreshTokenSetup(res);
  };

  const onFailure = (res) => {
    console.log("Login failed: res:", res);
  };
  useEffect(() => {
    setMounted(true);
  }, []);

  return (
    mounted && (
      <GoogleLogin
        clientId={clientId}
        buttonText={text}
        onSuccess={onSuccess}
        onFailure={onFailure}
        cookiePolicy={"single_host_origin"}
        isSignedIn={true}
        className={`rol-btn-auth ${className}`}
      />
    )
  );
}

export default Login;
