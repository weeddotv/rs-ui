import React, { useState } from "react";
import { useSelector, useDispatch } from "../../hooks";

const Notification = () => {
  const { notification } = useSelector((state) => ({
    notification: state.globalStore.notification,
  }));

  const { hideNotification } = useDispatch((state) => ({
    hideNotification: state.globalStore.hideNotification,
  }));

  return (
    <>
      {notification?.show ? (
        <div className="rs-notification share-popup">
          <div className="container-popup is--share">
            <button
              data-w-id="d94334ee-d168-2d58-e895-7d520f758d21"
              className="close-div"
              onClick={hideNotification}
            >
              <img
                src="/images/CrossSVG.svg"
                loading="lazy"
                alt="Close down send and transfer popup"
              />
            </button>
            <h3 className="heading-10 rs-red">{notification?.title}</h3>
            <p>{notification?.description}</p>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default Notification;
