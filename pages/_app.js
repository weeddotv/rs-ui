import "antd/dist/antd.css";
import "../public/css/normalize.css";
import "../public/css/components.css";
import "../public/css/rollingstone.css";
import "../styles/common.scss";

import App from "next/app";
import { Provider } from "react-redux";
import loadingPlugin from "@rematch/loading";
import selectPlugin from "@rematch/select";

import { init } from "@rematch/core";
import * as models from "../models";
import { useScript } from "../hooks";
import { Notification } from "../components";
import { useEffect } from "react";
import { useRouter } from "next/router";

const store = init({
  plugins: [loadingPlugin(), selectPlugin()],
  models,
});

function MyApp({ Component, pageProps }) {
  const Router = useRouter();

  useScript("/js/rollingstone.js");
  useScript("/js/custom.js");
  useEffect(() => {
    if (window.Webflow) {
      window.Webflow.destroy();
      window.Webflow.ready();
      window.Webflow.require("ix2").init();
      document.dispatchEvent(new Event("readystatechange"));
    }
  }, [Router]);

  return (
    <Provider store={store}>
      <Notification />
      <Component {...pageProps} />
    </Provider>
  );
}
MyApp.getInitialProps = async (appContext) => {
  const appProps = await App.getInitialProps(appContext);
  return { ...appProps };
};

export default MyApp;
