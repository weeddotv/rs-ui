import { DefaultLayout } from "../../layouts";
import { PackItemDetail, NFTItem } from "../../components";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "../../hooks";
import { formatIPFSImage } from "../../utils/global";
import { UserService } from "../../services";

export default function PackDetail() {
  const Router = useRouter();

  const { packDetail, packNfts } = useSelector((state) => ({
    packDetail: state.packStore.packDetail,
    packNfts: state.packStore.packNfts,
  }));

  const { getPackDetail } = useDispatch((state) => ({
    getPackDetail: state.packStore.getPackDetail,
  }));

  useEffect(() => {
    if (Router.query.productId) {
      getPackDetail({ productId: Router.query.productId });
    }
  }, []);

  useEffect(() => {
    if (window.Webflow) {
      window.Webflow.destroy();
      window.Webflow.ready();
      window.Webflow.require("ix2").init();
      document.dispatchEvent(new Event("readystatechange"));
    }
  }, [packDetail, packNfts]);

  const handleOnClick = async (id) => {
    try {
      await UserService.buy(id);
    } catch (e) {}
  };
  return (
    <DefaultLayout>
      <div className="overflow is--white">
        <div className="section is---white">
          {packDetail ? (
            <PackItemDetail
              productId={packDetail?._id}
              name={packDetail?.atomicassets?.immutable_data?.name}
              price={packDetail?.price}
              amount={packDetail?.atomicassets.issued_supply}
              remainAmountText={`${
                packDetail?.atomicassets?.template_mint || 0
              } of ${packDetail?.atomicassets.issued_supply} left`}
              imageURL={formatIPFSImage(
                packDetail?.atomicassets?.immutable_data?.img
              )}
              description={packDetail?.atomicassets?.immutable_data?.des}
              soldOut={
                packDetail?.atomicassets?.issued_supply ===
                packDetail?.atomicassets?.template_mint
              }
              handleOnClick={handleOnClick}
            />
          ) : null}
        </div>

        <div className="section">
          <div className="container is-larger">
            <div className="heading-left">
              <h2 className="heading-7">NFT&#x27;s in this collection</h2>
              <div className="collection-list-wrapper-6 w-dyn-list">
                <div role="list" className="collection-list-7 w-dyn-items">
                  {packNfts?.map((item, index) => (
                    <NFTItem
                      key={item?.template_id?.toString() + index.toString()}
                      imageURL={formatIPFSImage(
                        item?.atomicassets?.immutable_data.img
                      )}
                      score={item?.atomicassets?.immutable_data?.score || 0}
                      NFTName={item?.atomicassets?.immutable_data?.name}
                      amountText={`${
                        (item?.odds * 100) / item?.total_odds
                      }% chance`}
                      description={item?.atomicassets?.immutable_data?.des}
                    />
                  ))}
                </div>

                {!!packNfts && packNfts.length === 0 ? (
                  <div className="w-dyn-empty">
                    <div>No items found.</div>
                  </div>
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
