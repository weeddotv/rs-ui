import { useEffect, useState } from "react";
import { DefaultLayout } from "../../layouts";
import { UserService } from "../../services";
import { LeaderBoardItem, Loading } from "../../components";

export default function Leaderboard() {
  const [rankList, setRankList] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const res = await UserService.getRankList();
      setRankList(res?.content);
    };
    fetchData();
  }, []);

  return (
    <DefaultLayout>
      <div className="overflow">
        <div className="section">
          <div className="container is--left is--hero">
            <div className="div-leader-content">
              <img
                src="images/RS-RecordSVG.svg"
                loading="lazy"
                data-w-id="9c079e85-b034-dd18-f843-c2fae943b6b9"
                alt="A Rolling Stone Australia record"
                className="record-404 animation"
              />
              <div className="rs-leader-heading">
                <h1 className="heading-14">Leaderboard</h1>
                <p className="paragraph-11">
                  The more collectors points you have the more likely you will
                  win access to celebrity filled events and exclusive rewards,
                  your collectors rank represents your probability of reward
                  here, so there is something for everyone not just the 1%.
                </p>
              </div>
            </div>

            {rankList?.length > 0 ? (
              <div className="leaderboard">
                {rankList?.map((item) => (
                  <LeaderBoardItem
                    key={item?.userId}
                    imageURL={item?.avatar}
                    email={item?.email}
                    instagramURL={item?.instagram}
                    twitterURL={item?.twitter}
                    score={item?.score}
                    rank={item?.rank}
                    stashURL={`/${item?.userId}`}
                  />
                ))}
              </div>
            ) : (
              <Loading />
            )}
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
