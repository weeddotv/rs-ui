import { DefaultLayout } from "../../../layouts";
import styled from "styled-components";
import { useDispatch, useSelector } from "../../../hooks";
import { Button, NFTItem } from "../../../components";
import { useEffect, useState } from "react";
import Router from "next/router";
import { formatIPFSImage } from "../../../utils/global";

const PackOpenedStyled = styled.div`
  ~ .footer {
    display: none;
  }
`;
export default function PackOpened() {
  const { openedNfts, user } = useSelector((state) => ({
    openedNfts: state.packStore.openedNfts,
    user: state.authStore.user,
  }));
  const { getOpenedNfts } = useDispatch((state) => ({
    getOpenedNfts: state.packStore.getOpenedNfts,
  }));

  useEffect(() => {
    getOpenedNfts({ assetId: Router?.query?.assetId });
  }, [Router]);

  useEffect(() => {
    if (window.Webflow) {
      window.Webflow.destroy();
      window.Webflow.ready();
      window.Webflow.require("ix2").init();
      document.dispatchEvent(new Event("readystatechange"));
    }
  }, [openedNfts]);

  return (
    <DefaultLayout>
      <PackOpenedStyled>
        <div className="overflow">
          <div className="section">
            <div className="container is-centre is--bottom">
              <div className="centre-contents is--centred">
                <h1 className="heading-9 is--open">Pack opened</h1>
                <Button href={`/${user?._id}`}>BACK TO STASH</Button>
              </div>
              <div className="collection-list-wrapper-6 w-dyn-list">
                <div role="list" className="collection-list-7 w-dyn-items">
                  {openedNfts?.map((item, index) => (
                    <NFTItem
                      key={item?.template_id?.toString() + index.toString()}
                      imageURL={formatIPFSImage(item?.atomicassets?.data.img)}
                      score={item?.atomicassets?.data?.score || 0}
                      NFTName={item?.atomicassets?.data?.name}
                      // amountText={`${item?.atomicassets?.template_mint} of ${item?.atomicassets?.template?.issued_supply}`}
                      description={item?.atomicassets?.data?.des}
                    />
                  ))}
                </div>
                {openedNfts && !openedNfts?.length ? (
                  <div className="w-dyn-empty">
                    <div>No items found.</div>
                  </div>
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </PackOpenedStyled>
    </DefaultLayout>
  );
}
