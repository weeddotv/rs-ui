import { DefaultLayout } from "../../layouts";
import { PackItemDetail, NFTItem, Loading } from "../../components";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { useDispatch, useSelector } from "../../hooks";
import { formatIPFSImage } from "../../utils/global";
import { UserService } from "../../services";

export default function PackDetail() {
  const Router = useRouter();

  const { packDetail, packNfts } = useSelector((state) => ({
    packDetail: state.packStore.packDetail,
    packNfts: state.packStore.packNfts,
  }));
  const {
    getPackDetail,
    setPackDetail,
    openLoading,
    closeLoading,
    showNotification,
  } = useDispatch((state) => ({
    getPackDetail: state.packStore.getPackDetail,
    setPackDetail: state.packStore.setPackDetail,
    openLoading: state.globalStore.openLoading,
    closeLoading: state.globalStore.closeLoading,
    showNotification: state.globalStore.showNotification,
  }));

  useEffect(() => {
    if (Router.query.productId) {
      getPackDetail({ productId: Router.query.productId });
    }
    return () => {
      setPackDetail(null);
    };
  }, []);

  useEffect(() => {
    if (window.Webflow) {
      window.Webflow.destroy();
      window.Webflow.ready();
      window.Webflow.require("ix2").init();
      document.dispatchEvent(new Event("readystatechange"));
    }
  }, [packDetail, packNfts]);

  const handleOnClick = async (id) => {
    try {
      openLoading();
      const res = await UserService.buy(id);
      window.location.assign(res.url);
      closeLoading();
    } catch (e) {
      closeLoading();

      showNotification({
        title: "Error",
        description: "Authorization token has not been specified",
      });
    }
  };
  return (
    <DefaultLayout>
      <div className="overflow is--white">
        <div className="section is---white">
          {packDetail ? (
            <PackItemDetail
              productId={packDetail?._id}
              name={packDetail?.atomicassets?.immutable_data?.name}
              price={packDetail?.price}
              amount={packDetail?.atomicassets.issued_supply}
              remainAmountText={`${packDetail?.available || 0} of ${
                packDetail?.atomicassets.issued_supply
              } left`}
              imageURL={formatIPFSImage(
                packDetail?.atomicassets?.immutable_data?.img
              )}
              description={packDetail?.atomicassets?.immutable_data?.des}
              soldOut={packDetail?.soldOut}
              handleOnClick={handleOnClick}
            />
          ) : (
            <Loading minHeight="100vh" />
          )}
        </div>

        <div className="section">
          <div className="container is-larger">
            <div className="heading-left">
              <h2 className="heading-7">NFT&#x27;s in this collection</h2>

              <div className="collection-list-wrapper-6 w-dyn-list">
                {!packNfts ? (
                  <Loading minHeight="100vh" />
                ) : (
                  <div role="list" className="collection-list-7 w-dyn-items">
                    {packNfts?.map((item, index) => (
                      <NFTItem
                        key={item?.template_id?.toString() + index.toString()}
                        imageURL={formatIPFSImage(
                          item?.atomicassets?.immutable_data.img
                        )}
                        score={item?.atomicassets?.immutable_data?.score || 0}
                        NFTName={item?.atomicassets?.immutable_data?.name}
                        amountText={`${
                          (item?.odds * 100) / item?.total_odds
                        }% chance`}
                        description={item?.atomicassets?.immutable_data?.des}
                      />
                    ))}
                  </div>
                )}

                {!!packNfts && packNfts.length === 0 ? (
                  <div className="w-dyn-empty">
                    <div>No items found.</div>
                  </div>
                ) : null}
              </div>
            </div>
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
