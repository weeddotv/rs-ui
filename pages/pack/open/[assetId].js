import { DefaultLayout } from "../../../layouts";
import styled from "styled-components";
import { useSelector, useDispatch } from "../../../hooks";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { UserService } from "../../../services";

const PackOpenStyled = styled.div`
  .overflow {
    min-height: 100vh;
    padding-bottom: 80px;
    .back-div {
      button {
        background-color: transparent;
        color: #d32531;
        font-size: 1em;
        text-decoration: none;
      }
    }
  }
  ~ .footer {
    display: none;
  }
`;
export default function PackOpen() {
  const Router = useRouter();
  const [unPackData, setUnPackData] = useState(null);
  const { openPackSelected, user } = useSelector((state) => ({
    openPackSelected: state.packStore.openPackSelected,
    user: state.authStore.user,
  }));

  const { showNotification } = useDispatch((state) => ({
    showNotification: state.globalStore.showNotification,
  }));

  useEffect(() => {
    if (!openPackSelected?.imageURL) {
      Router.push("/");
    }
  }, []);

  useEffect(() => {
    if (window.Webflow) {
      window.Webflow.destroy();
      window.Webflow.ready();
      window.Webflow.require("ix2").init();
      document.dispatchEvent(new Event("readystatechange"));
    }
  }, [openPackSelected, unPackData]);

  const onOpenPack = async () => {
    setUnPackData({
      loading: true,
    });
    try {
      const res = await UserService.unPack(Router?.query?.assetId);
      const checkUnpackAssetTimer = setInterval(async () => {
        const res = await UserService.getUnPackRes(Router?.query?.assetId);
        if (res?.unpackedAsset?.status === 4) {
          clearInterval(checkUnpackAssetTimer);
          setUnPackData({
            ...unPackData,
            loading: false,
          });
          Router.push(`/pack/opened/${Router?.query?.assetId}`);
        }
      }, 1000);
    } catch (e) {
      setUnPackData({
        ...unPackData,
        loading: false,
      });

      showNotification({
        title: "Error",
        description: "Authorization token has not been specified",
      });
    }
  };
  return (
    <DefaultLayout>
      <PackOpenStyled>
        <div className="overflow">
          {openPackSelected ? (
            <div className="section">
              <div className="back-div">
                <p className="paragraph-4">
                  <Link href={`/${user?._id}`}>
                    <a className="link-3">&lt; Back to stash</a>
                  </Link>
                </p>
              </div>
              <div className="img-pack">
                <div
                  data-w-id="933ee90b-bfdd-748f-08f7-aedbefb3dd31"
                  data-animation-type="lottie"
                  data-src="/documents/data.json"
                  data-loop="1"
                  data-direction="1"
                  data-autoplay="1"
                  data-is-ix2-target="0"
                  data-renderer="svg"
                  data-default-duration="2.44"
                  data-duration="3"
                  className="lottie-animation-3"
                ></div>

                {/* animation here */}
                <img
                  src={openPackSelected?.imageURL}
                  loading="lazy"
                  alt="An epic collectable pack"
                  className={`pack-to-open ${
                    unPackData?.loading ? "rol-jiggle" : ""
                  }`}
                />
                <div className="main-left-button is--open">
                  <button
                    data-w-id="3f6150e0-1760-3eda-c01d-1207fa829e75"
                    className="button-top w-inline-block"
                    onClick={onOpenPack}
                  >
                    <div className="button-text">OPEN PACK</div>
                  </button>
                  <div className="button-back"></div>
                </div>
              </div>
            </div>
          ) : null}
        </div>
      </PackOpenStyled>
    </DefaultLayout>
  );
}
