import { DefaultLayout } from "../../layouts";

export default function FAQ() {
  return (
    <DefaultLayout>
      <div className="overflow">
        <div className="section">
          <div className="container is--left is--hero">
            <h1>FAQ</h1>
            <div className="rich-text-block-3 w-richtext">
              <h3>What is an NFT?</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </p>
              <h3>Do you do refunds?</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </p>
              <h3>Where can I go to find past collections</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </p>
              <h3>Creative commons attributions</h3>
              <p>
                <a
                  href="https://lottiefiles.com/734-circle-grow"
                  target="_blank"
                >
                  Circle Grow animation by Jean Sala on LottieFiles
                </a>
              </p>
              <p>
                <a
                  href="https://lottiefiles.com/27582-hamburger-menu"
                  target="_blank"
                >
                  Hamburger Menu animation by Oz Hashimoto on LottieFiles
                </a>
              </p>
              <p>&zwj;</p>
              <p>&zwj;</p>
              <p>&zwj;</p>
            </div>
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
