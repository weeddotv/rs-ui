import { DefaultLayout } from "../../layouts";

export default function Privacy() {
  return (
    <DefaultLayout>
      <div className="overflow">
        <div className="section">
          <div className="container is--left is--hero">
            <h1>Privacy</h1>
            <div className="rich-text-block-3 w-richtext">
              <h3>Point 1</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </p>
              <h3>Point 2</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </p>
              <p>&zwj;</p>
            </div>
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
