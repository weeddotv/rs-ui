import { DefaultLayout } from "../../layouts";

export default function AboutUs() {
  return (
    <DefaultLayout>
      <div className="overflow">
        <div className="section">
          <div className="container is--left is--hero">
            <h1>About us</h1>
            <div className="rich-text-block-3 w-richtext">
              <h3>Our history</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </p>
              <h3>Who are we</h3>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat.
              </p>
              <p>&zwj;</p>
            </div>
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
