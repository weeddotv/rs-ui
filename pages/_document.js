import Document, { Html, Head, Main, NextScript } from "next/document";
import Script from "next/script";

export default class MyDocument extends Document {
  render() {
    return (
      <Html
        lang="en"
        data-wf-page="611c8110cdf855d8158eb018"
        data-wf-site="611c8104e15cb659481fb48d"
      >
        <Head>
          <meta name="mobile-web-app-capable" content="yes" />
          {/* <meta
            name="google-signin-client_id"
            content="302930502755-rlrmctr8ms4mrulbrp3v09es65300npg.apps.googleusercontent.com"
          /> */}

          <link
            rel="shortcut icon"
            href="/images/favicon.png"
            type="image/x-icon"
          />
          <link
            rel="icon"
            href="/images/favicon.png"
            type="image/png"
            sizes="32x32"
          />
          <link
            rel="icon"
            href="/images/favicon.png"
            type="image/png"
            sizes="48x48"
          />
          <link
            rel="icon"
            href="/images/favicon.png"
            type="image/png"
            sizes="96x96"
          />
          <link
            rel="icon"
            href="/images/favicon.png"
            type="image/png"
            sizes="144x144"
          />
          <link
            rel="icon"
            href="/images/favicon.png"
            type="image/png"
            sizes="152x152"
          />
          <link
            rel="icon"
            href="/images/favicon.png"
            type="image/png"
            sizes="180x180"
          />
          <link
            rel="icon"
            href="/images/favicon.png"
            type="image/png"
            sizes="120x120"
          />

          <link
            rel="apple-touch-icon-precomposed"
            href="/images/favicon.png"
            sizes="72×72"
          />
          <link
            rel="apple-touch-icon-precomposed"
            href="/images/favicon.png"
            sizes="114×114"
          />
          <link
            rel="apple-touch-icon-precomposed"
            href="/images/favicon.png"
            sizes="120×120"
          />
          <link
            rel="apple-touch-icon-precomposed"
            href="/images/favicon.png"
            sizes="144×144"
          />
          <link
            rel="apple-touch-icon-precomposed"
            href="/images/favicon.png"
            sizes="152×152"
          />
          <link
            rel="apple-touch-icon-precomposed"
            href="/images/favicon.png"
            sizes="167×167"
          />
          <link
            rel="apple-touch-icon-precomposed"
            href="/images/favicon.png"
            sizes="180×180"
          />
          <link
            rel="stylesheet"
            type="text/css"
            charSet="UTF-8"
            href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.min.css"
          />
          <link
            rel="stylesheet"
            type="text/css"
            href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick-theme.min.css"
          />
          <script src="https://d3e54v103j8qbb.cloudfront.net/js/jquery-3.5.1.min.dc5e7f18c8.js?site=611c8104e15cb659481fb48d"></script>
          <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        </Head>
        <body className="body">
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
