import { StashItem, SendTradePopup, SharePopup, Loading } from "../components";
import { DefaultLayout } from "../layouts";
import { useDispatch, useSelector } from "../hooks";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import Link from "next/link";
import { formatIPFSImage } from "../utils/global";
import { UserService } from "../services";

export default function Stash() {
  const Router = useRouter();
  const [sendTradeData, setSendTradeData] = useState(null);
  const [shareData, setShareData] = useState(null);
  const [isOwner, setIsOwner] = useState(false);
  const { stash, userInfo, user } = useSelector((state) => ({
    stash: state.stashStore.stash,
    userInfo: state.stashStore.userInfo,
    user: state.authStore.user,
  }));

  const { getStashByUserId, getUserInfo, setUserInfo, setStash } = useDispatch(
    (state) => ({
      getStashByUserId: state.stashStore.getStashByUserId,
      getUserInfo: state.stashStore.getUserInfo,
      setUserInfo: state.stashStore.setUserInfo,
      setStash: state.stashStore.setStash,
    })
  );

  useEffect(() => {
    if (user && Router?.query?.userId !== user?.userId) {
      getUserInfo({ userId: Router?.query?.userId });
    }
    return () => {
      setUserInfo(null);
    };
  }, [Router, user]);

  useEffect(() => {
    getStashByUserId({ userId: Router?.query?.userId });
    return () => {
      setUserInfo(null);
    };
  }, [Router]);

  useEffect(() => {
    if (Router?.query?.userId === user?.userId) {
      setIsOwner(true);
    }

    return () => {
      setUserInfo(null);
    };
  }, [user, Router]);

  const openSendPopup = (assetId) => {
    setSendTradeData({
      ...sendTradeData,
      assetId: assetId,
      open: true,
    });
  };

  const onClose = () => {
    setSendTradeData({
      open: false,
    });
  };
  const onSend = async (values) => {
    setSendTradeData({
      ...sendTradeData,
      loading: true,
    });

    try {
      const res = await UserService.transfer(values);
      if (res) {
        setSendTradeData({
          ...sendTradeData,
          loading: false,
          success: true,
        });

        setStash(stash.filter((item) => item.assetId !== res.assetId));
      }
    } catch (e) {
      setSendTradeData({
        ...sendTradeData,
        loading: false,
        success: false,
      });
    }
  };

  const onShare = () => {
    setShareData({
      isOpen: true,
      url: window.location.href,
    });
  };
  const onShareClose = () => {
    setShareData({
      isOpen: false,
      url: null,
    });
  };

  return (
    <DefaultLayout>
      <div className="overflow rol-s-stash">
        <div className="section">
          {sendTradeData?.open ? (
            <SendTradePopup
              onSend={onSend}
              onClose={onClose}
              sendTradeData={sendTradeData}
            />
          ) : null}
          {shareData?.isOpen ? (
            <SharePopup url={shareData?.url} onClose={onShareClose} />
          ) : null}

          <div className="container is-centre is--bottom">
            <div className="centre-contents">
              <div className="picture-div">
                <img
                  src={
                    isOwner
                      ? user?.avatar
                      : userInfo?.avatar || "/images/defaultAvatar.svg"
                  }
                  loading="lazy"
                  alt="Avatar for user account page"
                  className="image-8 is---space rol-avatar"
                />
              </div>
              <div className="personal-collect">
                <div className="personal-div">
                  <p className="paragraph-4">
                    {isOwner ? user?.email : userInfo?.email}
                  </p>
                  {isOwner ? (
                    <>
                      <p className="paragraph-4">|</p>
                      <p className="paragraph-4">
                        <button onClick={onShare} className="link">
                          share
                        </button>
                      </p>
                    </>
                  ) : null}
                </div>
                <div className="personal-div">
                  <img
                    src="/images/Collect.svg"
                    loading="lazy"
                    alt="Collectors score item"
                    className="image-13"
                  />
                  <div className="text-block-2">Collector score: </div>
                  <Link href="/leaderboard">
                    <a
                      style={{
                        marginLeft: "5px",
                        fontWeight: "700",
                        fontSize: "1.6em",
                      }}
                    >
                      {isOwner ? user?.score : userInfo?.score}
                    </a>
                  </Link>{" "}
                </div>
                <div className="personal-div">
                  {userInfo?.instagram ? (
                    <a
                      href={userInfo?.instagram}
                      className="link-block-4 w-inline-block"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <img
                        src="images/Instagram.svg"
                        loading="lazy"
                        alt="Instagram icon"
                        className="image-13"
                      />
                      <div className="text-block-2">Instagram</div>
                    </a>
                  ) : null}
                  {userInfo?.twitter ? (
                    <a
                      href={userInfo?.twitter}
                      className="link-block-4 w-inline-block"
                      target="_blank"
                      rel="noreferrer"
                    >
                      <img
                        src="images/Twitter.svg"
                        loading="lazy"
                        alt="Twitter icon"
                        className="image-13"
                      />
                      <div className="text-block-2">Twitter</div>
                    </a>
                  ) : null}
                </div>
              </div>
              <h1 className="heading-9 is--hero">NFT Stash</h1>
            </div>

            {!stash ? (
              <Loading />
            ) : (
              <div className="collection-list-wrapper-6 w-dyn-list">
                <div role="list" className="collection-list-7 w-dyn-items">
                  {stash?.map((item) => (
                    <StashItem
                      key={item?.assetId}
                      packable={item?.packable}
                      imageURL={formatIPFSImage(item?.atomicassets.data?.img)}
                      stashURL={`/stash/${item?.assetId}`}
                      score={item?.atomicassets.data?.score || 0}
                      stashName={item?.atomicassets.data?.name}
                      remainAmountText={`${
                        item?.atomicassets?.template_mint || 0
                      } of ${item?.atomicassets.template?.issued_supply}`}
                      description={item?.atomicassets.data?.des}
                      assetId={item?.assetId}
                      openURL={`/pack/open/${item?.assetId}`}
                      redeemURL={`/redeem/${item?.assetId}`}
                      sendStradeOnclick={openSendPopup}
                      isOwner={isOwner}
                      redeemAble={item?.atomicassets.data?.redeemable}
                    />
                  ))}
                </div>
                {!!stash && stash.length === 0 ? (
                  <div className="w-dyn-empty">
                    <div>No items found.</div>
                  </div>
                ) : null}
              </div>
            )}
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
