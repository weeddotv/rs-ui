import { DefaultLayout } from "../../layouts";
import { Button } from "../../components";
import { useSelector } from "../../hooks";
import styled from "styled-components";
import { useEffect, useState } from "react";
const CancelPapeStyled = styled.div`
  padding: 140px 0 80px;
  text-align: center;
  min-height: 100vh;
  h1 {
    font-size: 60px;
    margin-bottom: 60px;
  }
  .rol-btn-back {
    margin: 0 auto;
  }
  @media (min-width: 768px) {
    padding: 240px 0 80px;
  }
`;
export default function Cancel() {
  const [loading, setLoading] = useState(true);
  const { user } = useSelector((state) => ({
    user: state.authStore.user,
  }));

  useEffect(() => {
    setLoading(false);
  }, []);

  return (
    <DefaultLayout>
      <CancelPapeStyled>
        {!loading ? (
          <>
            <h1>Payment cancelled</h1>
            <Button href={`/${user?._id}`} className="rol-btn-back">
              Back to stash
            </Button>
          </>
        ) : null}
      </CancelPapeStyled>
    </DefaultLayout>
  );
}
