import { DefaultLayout } from "../../layouts";
import { Button, StashItem, Loading } from "../../components";
import styled from "styled-components";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { useSelector, useDispatch } from "../../hooks";
import { formatIPFSImage } from "../../utils/global";

const SuccessPapeStyled = styled.div`
  padding: 140px 0 80px;
  text-align: center;
  min-height: 100vh;
  h1 {
    font-size: 60px;
    margin-bottom: 60px;
  }
  .rol-btn-back {
    margin: 0 auto;
  }
  @media (min-width: 768px) {
    padding: 240px 0 80px;
  }
`;
export default function Success() {
  const Router = useRouter();

  const [loading, setLoading] = useState(true);
  const { user, packDetail } = useSelector((state) => ({
    user: state.authStore.user,
    packDetail: state.packStore.packDetail,
  }));
  const { getPackDetail, setPackDetail } = useDispatch((state) => ({
    getPackDetail: state.packStore.getPackDetail,
    setPackDetail: state.packStore.setPackDetail,
  }));
  useEffect(() => {
    setLoading(false);
  }, []);

  useEffect(() => {
    if (Router.query.id) {
      getPackDetail({ productId: Router.query.id });
    }
    return () => {
      setPackDetail(null);
    };
  }, [Router]);
  return (
    <DefaultLayout>
      <SuccessPapeStyled>
        {!loading ? (
          <>
            <h1>Payment Successful</h1>
            {user?._id ? (
              <Button href={`/${user?._id}`} className="rol-btn-back">
                Back to stash
              </Button>
            ) : null}
          </>
        ) : null}

        {!packDetail ? (
          <Loading />
        ) : (
          <div className="collection-list-wrapper-6 w-dyn-list pd-top-bottom">
            <div role="list" className="collection-list-7 w-dyn-items">
              <StashItem
                key={packDetail?.assetId}
                packable={packDetail?.packable}
                imageURL={formatIPFSImage(
                  packDetail?.atomicassets.immutable_data?.img
                )}
                stashURL={`/stash/${packDetail?.assetId}`}
                score={packDetail?.atomicassets.immutable_data?.score || 0}
                stashName={packDetail?.atomicassets.immutable_data?.name}
                description={packDetail?.atomicassets.immutable_data?.des}
                assetId={packDetail?.assetId}
                openURL={`/pack/open/${packDetail?.assetId}`}
                redeemURL={`/redeem/${packDetail?.assetId}`}
                // sendStradeOnclick={openSendPopup}
                redeemAble={packDetail?.atomicassets.immutable_data?.redeemable}
              />
            </div>
          </div>
        )}
      </SuccessPapeStyled>
    </DefaultLayout>
  );
}
