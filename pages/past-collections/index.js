import { useEffect, useState } from "react";
import { BlockCollection, Loading } from "../../components";
import { DefaultLayout } from "../../layouts";
import { useDispatch, useSelector } from "../../hooks";
import moment from "moment";
import { formatIPFSImage } from "../../utils/global";

export default function Archive() {
  const [loading, setLoading] = useState(true);
  const { collectionsArchive } = useSelector((state) => ({
    collectionsArchive: state.collectionStore.collectionsArchive,
  }));

  const { getCollectionArchive } = useDispatch((state) => ({
    getCollectionArchive: state.collectionStore.getCollectionArchive,
  }));

  useEffect(() => {
    getCollectionArchive();
  }, []);

  useEffect(() => {
    if (collectionsArchive) {
      setLoading(false);
    }
  }, [collectionsArchive]);

  return (
    <DefaultLayout>
      <div className="overflow">
        <div className="section is---white is--hero">
          <div className="container is--narrower">
            {!collectionsArchive ? (
              <Loading />
            ) : (
              <div className="div-block-10">
                <div className="w-dyn-list">
                  <div role="list" className="w-dyn-items">
                    {collectionsArchive?.content?.map((item) => (
                      <BlockCollection
                        key={item._id}
                        imageURL={formatIPFSImage(item?.atomicassets?.img)}
                        title={item?.atomicassets?.name}
                        subTitle={`Sold out - ${moment(item?.endTime).format(
                          "MMMM YYYY"
                        )}`}
                        description="Browse the items sold this collection:"
                        showPack={true}
                      />
                    ))}

                    {!loading && !collectionsArchive?.content?.length ? (
                      <div className="w-dyn-empty">
                        <div>No items found.</div>
                      </div>
                    ) : null}
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
