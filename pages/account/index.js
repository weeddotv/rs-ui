import { useSelector, useDispatch } from "../../hooks";
import { DefaultLayout } from "../../layouts";
import { Spin } from 'antd';
import { LoadingOutlined } from "@ant-design/icons";

export default function Account() {
  const { user, loading } = useSelector((state) => ({
    user: state.authStore.user,
    loading: state.loading,
  }));
  const { updateMe } = useDispatch((state) => ({
    updateMe: state.authStore.updateMe,
  }));

  const updatePrivate = (currentPrivate) => {
    updateMe({ private: !currentPrivate });
  };

  return (
    <DefaultLayout>
      <div className="overflow">
        <div className="section">
          <div className="container is--account">
            <div className="centre-contents is-narrower">
              <div className="picture-div is---space">
                <img
                  src={user?.avatar || "/images/defaultAvatar.svg"}
                  loading="lazy"
                  alt="Avatar for user account page"
                  className="image-8 rol-avatar"
                />
              </div>
              {/* <div className="personal-div">
                <p className="paragraph-4">@{user?.userId}</p>
              </div> */}
            </div>
            <div className="centre-contents is--aligned">
              <div className="transaction-row is--accounts is-vert">
                <div className="transaction-text is-vert">Email:</div>
                <div className="transaction-text is-vert">{user?.email}</div>
              </div>
              {/* <div className="transaction-row is--accounts is-vert">
                <div className="transaction-text is-vert">Password:</div>
                <div className="transaction-text is-vert">*******</div>
                <a href="#" className="link is-narrower">
                  edit
                </a>
              </div> */}
              {/* <div className="transaction-row is--accounts is-vert">
                <div className="transaction-text is-vert">Address:</div>
                <div className="transaction-text is-vert">
                  10 Tree Street, Lake
                </div>
                <a href="#" className="link is-narrower is--vert">
                  edit
                </a>
              </div> */}
              {/* <div className="transaction-row is--accounts is-vert">
                <div className="transaction-text is-vert">Name :</div>
                <div className="transaction-text is-vert">@MrGreen</div>
                <a href="#" className="link is-narrower is--vert">
                  edit
                </a>
              </div> */}
              {user?.twitter ? (
                <div className="transaction-row is--accounts is-vert">
                  <div className="transaction-text is-vert">Twitter:</div>
                  <a href={user?.twitter} className="link is-narrower is--vert">
                    link
                  </a>
                </div>
              ) : null}
              {user?.instagram ? (
                <div className="transaction-row is--accounts is-vert">
                  <div className="transaction-text is-vert">Instagram</div>
                  <a
                    href={user?.instagram}
                    className="link is-narrower is--vert"
                  >
                    link
                  </a>
                </div>
              ) : null}

              <div className="transaction-row is--accounts is-vert is--info">
                <div className="div-block-15">
                  <div className="transaction-text is-vert">Private:</div>

                  <span className="link is-narrower is--vert">
                    <button
                      onClick={() => updatePrivate(user?.private)}
                      className="rs-custom-private"
                    >
                      {user?.private ? "on" : "off"}
                    </button>

                    {loading?.effects?.authStore?.updateMe ? (
                      <LoadingOutlined className="rs-spin" spin />
                    ) : null}
                  </span>
                </div>
                <p className="info">Turn on to hide from leaderboard</p>
              </div>
            </div>
            <div className="deposit-div">
              <h3 className="heading-12">Deposit NFT&#x27;s</h3>
              <p className="paragraph-9">
                To deposit NFT&#x27;s to your Rolling Stone account use this
                account name and memo in your external wallet
              </p>
            </div>
            <div className="deposit-details">
              <div className="transaction-row is--accounts is-vert">
                <div className="transaction-text is-vert">Account:</div>
                <div className="transaction-text is-vert">
                  {user?.depositAccount}
                </div>
                {/* <a href="#" className="link is-narrower is--vert">
                  copy
                </a> */}
              </div>
              <div className="transaction-row is--accounts">
                <div className="transaction-text is-vert">Memo:</div>
                <div className="transaction-text is-vert">{user?.memo}</div>
                {/* <a href="#" className="link is-narrower is--vert">
                  copy
                </a> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
