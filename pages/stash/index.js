import { useSelector } from "../../hooks";
import { useEffect } from "react";
import Router from "next/router";
export default function stashItem() {
  const { user } = useSelector((state) => ({
    user: state.authStore.user,
  }));

  useEffect(() => {
    if (user?.userId) {
      Router.push(`/${user?.userId}`);
    }
  }, []);

  return <div />;
}
