import { DefaultLayout } from "../../layouts";
import { useDispatch, useSelector } from "../../hooks";
import { NFTItem, Loading } from "../../components";
import { useEffect } from "react";
import Router from "next/router";
import { formatIPFSImage } from "../../utils/global";
export default function stashItem() {
  const { stashDetail } = useSelector((state) => ({
    stashDetail: state.stashStore.stashDetail,
  }));

  const { getStashDetail } = useDispatch((state) => ({
    getStashDetail: state.stashStore.getStashDetail,
  }));

  useEffect(() => {
    getStashDetail({ assetId: Router?.query?.assetId });
  }, []);

  const onBack = (e) => {
    e.preventDefault();
    Router.back();
  };

  useEffect(() => {
    if (window.Webflow) {
      window.Webflow.destroy();
      window.Webflow.ready();
      window.Webflow.require("ix2").init();
      document.dispatchEvent(new Event("readystatechange"));
    }
  }, [stashDetail]);

  return (
    <DefaultLayout>
      <div className="overflow">
        <div className="section">
          <div className="back-div">
            <p className="paragraph-4">
              <a href="#" onClick={onBack} className="link-3">
                &lt; Back to stash
              </a>
            </p>
          </div>
          <div className="container is-centre2">
            <div className="collection-list-wrapper-6 w-dyn-list">
              {stashDetail ? (
                <div role="list" className="collection-list-7 w-dyn-items">
                  {stashDetail ? (
                    <NFTItem
                      imageURL={formatIPFSImage(stashDetail?.data.img)}
                      score={stashDetail?.data?.score || 0}
                      NFTName={stashDetail?.data?.name}
                      amountText={`${stashDetail?.template_mint || 0} of ${
                        stashDetail?.template?.issued_supply
                      }`}
                      description={stashDetail?.data?.des}
                    />
                  ) : null}
                </div>
              ) : (
                <Loading />
              )}
            </div>
            {/* <div className="centre-contents is--aligned is--main">
              <h2 className="heading-11">Sales History</h2>
              <p>
                See more on {' '}
                <a
                  href="https://wax.atomichub.io/"
                  target="_blank"
                  rel="noreferrer"
                >
                  AtomicHub
                </a>
              </p>
              <img
                src="/images/Screen-Shot-2021-08-29-at-4.57.34-pm.png"
                loading="lazy"
                sizes="100vw"
                srcSet="
                /images/Screen-Shot-2021-08-29-at-4.57.34-pm-p-500.png   500w,
                /images/Screen-Shot-2021-08-29-at-4.57.34-pm-p-800.png   800w,
                /images/Screen-Shot-2021-08-29-at-4.57.34-pm-p-1080.png 1080w,
                /images/Screen-Shot-2021-08-29-at-4.57.34-pm-p-1600.png 1600w,
                /images/Screen-Shot-2021-08-29-at-4.57.34-pm-p-2000.png 2000w,
                /images/Screen-Shot-2021-08-29-at-4.57.34-pm.png        2210w
              "
                alt="Graph showing sales history atomic hub"
                className="image-graph"
              />
            </div> */}
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
