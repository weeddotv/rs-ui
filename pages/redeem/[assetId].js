import { DefaultLayout } from "../../layouts";
import { useRouter } from "next/router";
import { Form, Input } from "antd";
import { UserService } from "../../services";
import { useState } from "react";
import { useDispatch } from "../../hooks";

export default function GenericFormPage() {
  const Router = useRouter();
  const [form] = Form.useForm();
  const { openLoading, closeLoading } = useDispatch((state) => ({
    openLoading: state.globalStore.openLoading,
    closeLoading: state.globalStore.closeLoading,
  }));

  const [redeem, setRedeem] = useState(null);

  const onBack = (e) => {
    e.preventDefault();
    Router.back();
  };

  const onRedeem = async (values) => {
    setRedeem({
      loading: true,
    });
    try {
      openLoading();
      const res = await UserService.onRedeem(values);
      closeLoading();

      if (res) {
        setRedeem({
          loading: false,
          success: true,
        });
      }
    } catch (e) {
      closeLoading();
      setRedeem({
        loading: false,
        success: false,
      });
    }
  };

  if (!Router?.query?.assetId) return null;
  return (
    <DefaultLayout>
      <div className="checkout-div is--middle">
        <div className="forms-div is-generic">
          <a href="#" onClick={onBack} className="link-2">
            &lt; Back to Stash
          </a>
          <h1 className="head#ing-5">Redeem NFT</h1>
          <p className="paragraph-2">
            Enter your contact information and shipping details here and we will
            burn (destroy) your NFT and ship out your redeemable goods in no
            time. Check your confirmation email for more info!
          </p>
          <div className="redeem-form">
            <Form
              form={form}
              onFinish={onRedeem}
              initialValues={{
                assetId: Router?.query?.assetId,
              }}
            >
              <Form.Item name="assetId">
                <Input type="hidden" />
              </Form.Item>
              <Form.Item
                type="email"
                name="email"
                rules={[
                  { required: true, message: "Email is required" },
                  { type: "email", message: "Email is not a valid email" },
                ]}
              >
                <Input placeholder="Email Address" maxLength="256" />
              </Form.Item>
              <Form.Item
                name="name"
                rules={[{ required: true, message: "Full Name is required" }]}
              >
                <Input placeholder="Full Name" maxLength="256" />
              </Form.Item>
              <Form.Item
                name="address"
                rules={[{ required: true, message: "Address is required" }]}
              >
                <Input placeholder="Address" maxLength="256" />
              </Form.Item>
              <Form.Item
                name="phoneNumber"
                rules={[
                  { required: true, message: "Phone Number is required" },
                ]}
              >
                <Input placeholder="Phone Number" maxLength="256" />
              </Form.Item>
              <div className="main-left-button">
                <button
                  data-w-id={"3f6150e0-1760-3eda-c01d-1207fa829e75"}
                  className="button-top w-inline-block"
                >
                  <div className="button-text">Submit</div>
                </button>

                <div className="button-back"></div>
              </div>
            </Form>

            {redeem?.loading === false && redeem?.success === true ? (
              <div
                className="w-form-done"
                style={{ display: "block", fontSize: "30px" }}
              >
                Thank you! Your submission has been received!
              </div>
            ) : null}
            {redeem?.loading === false && redeem?.success === false ? (
              <div className="error-message">
                <div className="error-text">
                  Hmmm, something went wrong. Please try again!
                </div>
              </div>
            ) : null}
          </div>
          <p className="para-disclaimer">
            By submitting this form you agree to burning the NFT and confirming
            your contact and shipping information
          </p>
        </div>
      </div>
    </DefaultLayout>
  );
}
