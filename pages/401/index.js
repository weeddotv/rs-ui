export default function Page401() {
  return (
    <div className="utility-page-wrap">
      <div className="utility-page-content w-password-page w-form">
        <form method="post" className="utility-page-form w-password-page">
          <img
            src="https://d3e54v103j8qbb.cloudfront.net/static/utility-lock.ae54711958.svg"
            alt=""
          />
          <h2>Protected Page</h2>
          <label htmlFor="pass">Password</label>
          <input
            type="password"
            autoFocus="true"
            name="pass"
            placeholder="Enter your password"
            maxLength="256"
            className="w-password-page w-input"
          />
          <input
            type="submit"
            value="Submit"
            className="w-password-page w-button"
          />
          <div className="w-password-page w-form-fail">
            <div>Incorrect password. Please try again.</div>
          </div>
        </form>
      </div>
    </div>
  );
}
