import { useEffect, useState } from "react";
import { DefaultLayout } from "../layouts";
import { BLOCK_INFO } from "../utils/constants";
import Link from "next/link";
import Countdown from "react-countdown";
import {
  Button,
  LeaderBoardItem,
  PackItem,
  NFTItem,
  MagazineScroll,
  Block,
  BlockCollection,
  Loading,
} from "../components";
import { Form, Input, Button as ButtonAntd } from "antd";

import { useSelector, useDispatch } from "../hooks";
import { formatIPFSImage, formatListText } from "../utils/global";

import { UserService } from "../services";

export default function Home() {
  const [form] = Form.useForm();

  const [subcription, setSubcription] = useState(null);
  const { rankList, collectionSale, packsSale, nftsSale } = useSelector(
    (state) => ({
      rankList: state.rankListStore.rankList,
      collectionSale: state.collectionStore.collectionSale,
      packsSale: state.collectionStore.packsSale,
      nftsSale: state.collectionStore.nftsSale,
    })
  );

  const { getRankList, getCollectionSale, getPacksSale, getNftsSale } =
    useDispatch((state) => ({
      getRankList: state.rankListStore.getRankList,
      getCollectionSale: state.collectionStore.getCollectionSale,
      getPacksSale: state.collectionStore.getPacksSale,
      getNftsSale: state.collectionStore.getNftsSale,
    }));

  useEffect(() => {
    getRankList({ pageSize: 3 });
  }, []);

  useEffect(() => {
    getCollectionSale();
  }, []);

  useEffect(() => {
    if (collectionSale) {
      getPacksSale({ collectionId: collectionSale?._id });
    }
  }, [collectionSale]);

  useEffect(() => {
    if (packsSale) {
      getNftsSale({ packsSale });
    }
  }, [packsSale]);

  useEffect(() => {
    if (window.Webflow) {
      window.Webflow.destroy();
      window.Webflow.ready();
      window.Webflow.require("ix2").init();
      document.dispatchEvent(new Event("readystatechange"));
    }
  }, [packsSale, collectionSale, nftsSale]);

  const onSubscription = async (values) => {
    setSubcription({
      loading: true,
    });
    try {
      const res = await UserService.onSubscription(values?.email);
      if (res) {
        setSubcription({
          loading: false,
          success: true,
        });
      }
    } catch (e) {
      setSubcription({
        loading: false,
        success: false,
      });
    }
  };
  return (
    <DefaultLayout>
      <div className="overflow">
        <div className="section main">
          <div
            data-w-id="813226ac-0893-87cb-1757-f1e318c16db4"
            className="container-main"
          >
            <div className="main-left">
              <div className="main-left-h1">
                <img
                  src="/images/RS-RecordSVG.svg"
                  loading="lazy"
                  data-w-id="f032ea90-42d3-2ab5-224e-b233f0d003c6"
                  alt="A Rolling Stone Australia record"
                  className="main-left-record"
                />
                <h1 className="heading">
                  Own <span className="text-spacing">a</span>piece of pop
                  history.
                </h1>
              </div>
              <div className="main-left-paragraph">
                <p className="paragraph-2">
                  Every month Rolling Stone will release exclusive NFT’s with
                  every cover. For 50 years we’ve reported to you on culture,
                  now we&#x27;re giving you the chance to own it.
                </p>
              </div>

              {collectionSale ? (
                <>
                  <Button href="#collection-feature">START COLLECTING</Button>
                  <div className="main-left-arrow">
                    <img
                      src="/images/FullArrowSVG.svg"
                      loading="lazy"
                      alt="An arrow pointing to the status of the next Rolling Stone Collection offering"
                      className="arrowpoint"
                    />
                    <div className="text-status">
                      <div className="collection-name">
                        {collectionSale?.atomicassets?.name} NFT sale:
                      </div>
                      <div className="space"></div>
                      <div className="w-dyn-list">
                        <div role="list" className="w-dyn-items">
                          <div role="listitem" className="w-dyn-item">
                            <div className="collection-status">
                              {Date.now() -
                                new Date(collectionSale?.startTime).valueOf() >=
                              0 ? (
                                "live now"
                              ) : (
                                <Countdown
                                  date={new Date(
                                    collectionSale?.startTime
                                  ).valueOf()}
                                />
                              )}
                            </div>
                          </div>
                        </div>
                        {/* <div className="w-dyn-empty">
                          <div>No items found.</div>
                        </div> */}
                      </div>
                    </div>
                  </div>
                </>
              ) : null}
            </div>
            <div className="main-right">
              <img
                src="/images/Circle-v2SVG.svg"
                loading="lazy"
                alt="A circular piece of pop NFT history"
                className="image"
              />
              <img
                src="/images/JimTriangleSVG.svg"
                loading="lazy"
                alt="Jim playing the guitar"
                className="image-2"
              />
              <img
                src="/images/Triangle-v2SVG.svg"
                loading="lazy"
                alt="A triangle piece of pop NFT history"
                className="image-3"
              />
              <img
                src="/images/Fred3D-v2SVG.svg"
                loading="lazy"
                alt="Performing on stage"
                className="image-main"
              />
              <img
                src="/images/Blur.jpg"
                loading="lazy"
                alt="Billie Eilish piece of NFT"
                className="image-circle"
              />
            </div>
          </div>
        </div>
        <div className="section is---white">
          <div id="collection-feature" className="container collection-feature">
            <BlockCollection
              imageURL={formatIPFSImage(
                collectionSale?.atomicassets?.data?.img
              )}
              title={collectionSale?.atomicassets?.data?.name}
              subTitle="Exclusive NFT collection:"
              countDown={{ show: true, time: collectionSale?.startTime }}
              description={collectionSale?.atomicassets?.data?.description}
              showProduct={false}
              className="rol-col-sale"
            />

            <div className="collection-products">
              <div className="w-dyn-list">
                {!!packsSale?.length ? (
                  <div role="list" className="collection-list-2 w-dyn-items">
                    <div
                      role="listitem"
                      className="collection-item-2 w-dyn-item"
                    >
                      <div className="w-dyn-list">
                        <div
                          role="list"
                          className="collection-list-3 w-dyn-items rol-packs-sale"
                        >
                          {packsSale?.length ? (
                            packsSale?.map((item) => (
                              <PackItem
                                key={item?._id}
                                imageURL={formatIPFSImage(
                                  item?.atomicassets?.immutable_data?.img
                                )}
                                price={item?.price}
                                amount={item?.atomicassets.issued_supply}
                                remainAmountText={`${item?.available || 0} of ${
                                  item?.atomicassets.issued_supply
                                } left`}
                                packURL={`/pack/${item?._id}`}
                                description={formatListText(
                                  item?.atomicassets?.immutable_data?.des
                                )}
                              />
                            ))
                          ) : (
                            <Loading />
                          )}
                        </div>
                      </div>
                    </div>
                  </div>
                ) : null}

                {/* {!packsSaleLoading && !packsSale?.length ? (
                  <div className="w-dyn-empty">
                    <div>No items found.</div>
                  </div>
                ) : null} */}
              </div>
            </div>
          </div>
        </div>
        <div className="section">
          <div className="container">
            <div className="div-block-11">
              <h2 className="heading-6 is--centred">
                NFT&#x27;s in this collection
              </h2>
            </div>
            <div className="collection-list-wrapper-6 w-dyn-list">
              <div role="list" className="collection-list-7 w-dyn-items">
                {nftsSale?.length > 0 ? (
                  nftsSale?.map((item, index) => (
                    <NFTItem
                      key={item?.template_id?.toString() + index.toString()}
                      imageURL={formatIPFSImage(
                        item?.atomicassets?.immutable_data.img
                      )}
                      score={item?.atomicassets?.immutable_data?.score || 0}
                      NFTName={item?.atomicassets?.immutable_data?.name}
                      amountText={`${
                        (item?.odds * 100) / item?.total_odds
                      }% chance`}
                      description={item?.atomicassets?.immutable_data?.des}
                    />
                  ))
                ) : (
                  <Loading />
                )}
              </div>
            </div>
          </div>
        </div>
        <div className="section is--subscribe">
          <MagazineScroll />

          <div className="container is--left is--hero">
            <h2 className="heading-subscribe">
              Access to celebrity-filled events and rewards
            </h2>
            <p className="is-centre is--leadertext">
              Rolling Stone will be giving away VIP event access and exclusive
              rewards to <em>anyone</em> with an NFT. The higher value your
              collection score, the more likely you are to win. Importantly -
              everyone has a chance. Browse NFT&#x27;s of our top collectors
              here:
            </p>
            <div className="leaderboard">
              {rankList?.content?.map((item) => (
                <LeaderBoardItem
                  key={item?.userId}
                  imageURL={item?.avatar}
                  email={item?.email}
                  instagramURL={item?.instagram}
                  twitterURL={item?.twitter}
                  score={item?.score}
                  rank={item?.rank}
                  stashURL={`/${item?.userId}`}
                />
              ))}
            </div>
            <div className="div-block-17">
              <Button className="is--2emabove" href="/leaderboard">
                VIEW LEADERBOARD
              </Button>
            </div>
          </div>
        </div>
        <div className="section is--red">
          <div className="container is--nowidth">
            <div className="div-block-14">
              <h2 className="heading-6 is--centred">
                What is a Rolling Stone NFT?
              </h2>
              <p className="is-centre is--nfttext">
                Have more questions? Check out our
                <Link href="/faq">
                  <a target="_blank" className="black-link" rel="noreferrer">
                    {" "}
                    <strong>FAQ&#x27;s</strong>
                  </a>
                </Link>
              </p>
            </div>
            <div className="benefits">
              {BLOCK_INFO.map((item) => (
                <Block
                  key={item.id}
                  imageURL={item.imageURL}
                  imageAlt={item.imageAlt}
                  title={item.title}
                  description={item.description}
                />
              ))}
            </div>
          </div>
        </div>
        <div className="section">
          <div className="subscribe-div">
            <div className="heading-subscribe-div">
              <h2 className="heading-subscribe">
                Don’t miss future exclusive collections
              </h2>
              <div className="grid-2em-top-bottom">
                <p className="is-centre">
                  Every new Rolling Stone magazine will now feature a
                  simultaneous NFT collectors edition sale from one of the
                  greats we feature in the magazine. Sign up below to be the
                  first to know about it.
                </p>
              </div>
              <div className="get-alerted-form">
                <Form form={form} onFinish={onSubscription}>
                  <Form.Item
                    type="email"
                    name="email"
                    rules={[
                      { required: true, message: "Email is required" },
                      { type: "email", message: "Email is not a valid email" },
                    ]}
                  >
                    <Input placeholder="Your email" maxLength="256" />
                  </Form.Item>
                  <Form.Item shouldUpdate>
                    {() => (
                      <ButtonAntd
                        className="ov-btn full-width"
                        htmlType="submit"
                        loading={subcription?.loading}
                        disabled={
                          !!form
                            .getFieldsError()
                            .filter(({ errors }) => errors.length).length
                        }
                      >
                        GET ALERTED
                      </ButtonAntd>
                    )}
                  </Form.Item>
                </Form>

                {subcription?.loading === false &&
                subcription?.success === true ? (
                  <div className="success-message">
                    <div className="error-text">
                      Thank you! You&#x27;ll hear from us soon.
                    </div>
                  </div>
                ) : null}
                {subcription?.loading === false &&
                subcription?.success === false ? (
                  <div className="error-message-2 ">
                    <div className="error-text">
                      Hmmm, something went wrong. Please try again!
                    </div>
                  </div>
                ) : null}
              </div>
              <div className="text-block-5">
                We will only email you about upcoming collections
              </div>
            </div>
          </div>
        </div>
      </div>
    </DefaultLayout>
  );
}
