import {
  UserService,
  AtomicAssetService,
  BlockChainService,
} from "../services";

export const packStore = {
  state: {
    packDetail: null,
    packNfts: null,
    openPackSelected: null,
    openedNfts: null,
  },
  reducers: {
    setPackDetail: (state, payload) => {
      return {
        ...state,
        packDetail: payload,
      };
    },
    setPacksNft: (state, payload) => {
      return {
        ...state,
        packNfts: payload,
      };
    },
    setOpenPackSelected: (state, payload) => {
      return {
        ...state,
        openPackSelected: payload,
      };
    },
    setOpenedNfts: (state, payload) => {
      return {
        ...state,
        openedNfts: payload,
      };
    },
  },
  effects: ({ packStore }) => ({
    getPackDetail: async ({ productId }) => {
      try {
        // get pack by productID
        const pack = await UserService.getProductByID(productId);

        // find data of the pack
        const atomicassetsRes = await AtomicAssetService.getTemplateByIds(
          pack.templateId
        );
        packStore.setPackDetail({ ...pack, atomicassets: atomicassetsRes[0] });

        // find nft in above pack
        const nftRes = await BlockChainService.getNFTByPackID(pack?.packId);

        let NFTs = nftRes.rows[0].outcomes.filter(
          (item) => item.template_id !== -1
        );

        const nftAtomicassets = await AtomicAssetService.getTemplateByIds(
          NFTs.map((item) => item.template_id).toString()
        );

        NFTs = NFTs.map((item) => {
          return {
            ...item,
            total_odds: nftRes.rows[0].total_odds,
            atomicassets: nftAtomicassets.find(
              (nftItem) => +item.template_id === +nftItem.template_id
            ),
          };
        });

        packStore.setPacksNft(NFTs);
      } catch (e) {
        console.error("Error: getPacksSale", e);
      }
    },
    getOpenedNfts: async ({ assetId }) => {
      try {
        const res = await UserService.getUnPackRes(assetId);

        if (res?.result.map((item) => item?.assetId).toString()) {
          const atomicassetsRes = await AtomicAssetService.getAssetByIds(
            res?.result.map((item) => item?.assetId).toString()
          );
          const NFTs = res?.result?.map((item) => {
            return {
              ...item,
              atomicassets: atomicassetsRes.find(
                (nftItem) => +item.assetId === +nftItem.asset_id
              ),
            };
          });
          packStore.setOpenedNfts(NFTs);
        } else {
          packStore.setOpenedNfts([]);
        }
      } catch (e) {
        console.error("Error: getOpenedNft", e);
      }
    },
  }),
};
