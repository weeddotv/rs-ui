import { UserService } from "../services";

export const rankListStore = {
  state: {
    rankList: null,
  },
  reducers: {
    setRankList: (state, payload) => {
      return {
        ...state,
        rankList: payload,
      };
    },
  },
  effects: ({ rankListStore }) => ({
    getRankList: async (payload) => {
      try {
        const res = await UserService.getRankList(payload?.pageSize);
        rankListStore.setRankList(res);
      } catch (e) {}
    },
  }),
};
