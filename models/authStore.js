import { AuthService, UserService } from "../services";
export const authStore = {
  state: {
    user: null,
    googleAuth: null,
  },
  reducers: {
    setUser: (state, payload) => {
      return {
        ...state,
        user: payload,
      };
    },
    setGoogleAuth: (state, payload) => {
      return {
        ...state,
        googleAuth: payload,
      };
    },
  },
  effects: ({ authStore }) => ({
    getMe: async (_, state) => {
      try {
        // if (!AuthService.getToken()) return;
        // if (state?.authStore?.user) return;
        const user = await AuthService.getMe();
        authStore.setUser(user);
      } catch (err) {
        console.error("--- Token is not valid, set user to not login:", err);
        // clear token
        AuthService.setToken(null);
        authStore.setUser(null);
      }
    },
    updateMe: async (payload) => {
      try {
        const user = await UserService.updateMe({ ...payload });
        authStore.setUser(user);
      } catch (err) {
        console.error("--- updateMe error:", err);
      }
    },
  }),
};
