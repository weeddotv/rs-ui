export const globalStore = {
  state: {
    loading: false,
    notification: { show: false, title: null, description: null },
  },
  reducers: {
    openLoading: (state) => {
      return {
        ...state,
        loading: true,
      };
    },
    closeLoading: (state) => {
      return {
        ...state,
        loading: false,
      };
    },
    showNotification: (state, payload) => {
      return {
        ...state,
        notification: {
          show: true,
          title: payload?.title,
          description: payload?.description,
        },
      };
    },
    hideNotification: (state) => {
      return {
        ...state,
        notification: {
          show: false,
        },
      };
    },
  },
  effects: (dispatch) => ({
    // duplicatePost() {
    //     dispatch.posts.setPosts(POST_TEMPLATE)
    // },
  }),
};
