import { AtomicAssetService } from "../services";
import UserService from "../services/UserService";

export const stashStore = {
  state: {
    stash: null,
    stashDetail: null,
    stashOpen: null,
    userInfo: null,
  },
  reducers: {
    setStash: (state, payload) => {
      return {
        ...state,
        stash: payload,
      };
    },
    setStashDetail: (state, payload) => {
      return {
        ...state,
        stashDetail: payload,
      };
    },
    setUserInfo: (state, payload) => {
      return {
        ...state,
        userInfo: payload,
      };
    },
  },
  effects: ({ stashStore }) => ({
    getStashByUserId: async ({ userId }) => {
      try {
        let stashRes = [];
        const stash = await UserService.getStashByUserId(userId);

        if (stash.content.length > 0) {
          const packsInfo = await AtomicAssetService.getAssetByIds(
            stash.content.map((item) => item.assetId).toString()
          );
          stashRes = stash.content.map((item) => {
            return {
              ...item,
              atomicassets: packsInfo.find(
                (packsInfoItem) => packsInfoItem.asset_id === item.assetId
              ),
            };
          });
        }

        stashStore.setStash(stashRes);
      } catch (e) {
        console.error("Error: getStashByUserId", e);
      }
    },
    getStashDetail: async ({ assetId }) => {
      try {
        const stashDetail = await AtomicAssetService.getAssetByIds(assetId);

        stashStore.setStashDetail(stashDetail[0]);
      } catch (e) {
        console.error("Error: getStashDetail", e);
      }
    },
    getUserInfo: async ({ userId }) => {
      try {
        const userRes = await UserService.getUserInfo(userId);
        stashStore.setUserInfo(userRes);
      } catch (e) {
        console.error("Error: getUserInfo", e);
      }
    },
  }),
};
