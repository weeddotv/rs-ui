import {
  UserService,
  AtomicAssetService,
  BlockChainService,
} from "../services";

export const collectionStore = {
  state: {
    collectionSale: null,
    packsSale: null,
    nftsSale: null,
    collectionsArchive: null,
  },
  reducers: {
    setCollectionSale: (state, payload) => {
      return {
        ...state,
        collectionSale: payload,
      };
    },
    setPacksSale: (state, payload) => {
      return {
        ...state,
        packsSale: payload,
      };
    },
    setNftsSale: (state, payload) => {
      return {
        ...state,
        nftsSale: payload,
      };
    },
    setCollectionsArchive: (state, payload) => {
      return {
        ...state,
        collectionsArchive: payload,
      };
    },
  },
  effects: ({ collectionStore }) => ({
    getCollectionSale: async () => {
      try {
        const collection = await UserService.getCurrentSale();
        const collectInfo = await AtomicAssetService.getCollectionByName(
          collection?.collectionName
        );
        collectionStore.setCollectionSale({
          ...collection,
          atomicassets: collectInfo?.data,
        });
      } catch (e) {
        console.error("Error: getCollectionSale", e);
      }
    },
    getPacksSale: async ({ collectionId }) => {
      try {
        let packRes = [];
        const packs = await UserService.getProductsSale(collectionId);

        if (!!packs.content.length) {
          const packsInfo = await AtomicAssetService.getTemplateByIds(
            packs.content.map((item) => item.templateId).toString()
          );
          packRes = packs.content.map((packItem) => {
            return {
              ...packItem,
              atomicassets: packsInfo.find(
                (packsInfoItem) =>
                  packsInfoItem.template_id === packItem.templateId
              ),
            };
          });
        }

        collectionStore.setPacksSale(packRes);
      } catch (e) {
        console.error("Error: getPacksSale", e);
      }
    },

    getNftsSale: async ({ packsSale }) => {
      try {
        let NFTs = [];

        const getNFTs = packsSale.map(async (item) => {
          const nftRes = await BlockChainService.getNFTByPackID(item?.packId);
          NFTs = NFTs.concat(nftRes.rows);
        });

        Promise.all(getNFTs).then(() => {
          NFTs = NFTs.map((NFT) => {
            return NFT.outcomes.map((item) => ({
              ...item,
              total_odds: NFT.total_odds,
            }));
          });

          NFTs = [].concat.apply([], NFTs);

          AtomicAssetService.getTemplateByIds(
            NFTs.filter((item) => item.template_id !== -1)
              .map((item) => item.template_id)
              .toString()
          ).then((templateInfo) => {
            NFTs = NFTs.filter((item) => item.template_id !== -1).map(
              (templateItem) => {
                return {
                  ...templateItem,
                  atomicassets: templateInfo.find(
                    (templateInfoItem) =>
                      +templateInfoItem.template_id ===
                      +templateItem.template_id
                  ),
                };
              }
            );

            collectionStore.setNftsSale(NFTs);
          });
        });
      } catch (e) {
        console.error("Error: getNftsSale", e);
      }
    },
    getCollectionArchive: async () => {
      try {
        let collectionsAtomicassets = [];

        let collectionArchivesRes = await UserService.getArchive();

        if (collectionArchivesRes?.content.length > 0) {
          const getCollectInfo = collectionArchivesRes?.content.map(
            async (item) => {
              const atomicassets = await AtomicAssetService.getCollectionByName(
                item?.collectionName
              );

              collectionsAtomicassets = collectionsAtomicassets.concat(
                atomicassets?.data
              );
            }
          );

          Promise.all(getCollectInfo).then(() => {
            collectionArchivesRes = {
              ...collectionArchivesRes,
              content: collectionArchivesRes.content.map((item) => {
                return {
                  ...item,
                  atomicassets: collectionsAtomicassets.filter(
                    (collectionItem) =>
                      collectionItem.collection_name === item.collectionName
                  )[0],
                };
              }),
            };
            collectionStore.setCollectionsArchive(collectionArchivesRes);
          });
        } else {
          collectionStore.setCollectionsArchive(null);
        }
      } catch (e) {
        console.error("Error: getCollectionSale", e);
      }
    },
  }),
};
