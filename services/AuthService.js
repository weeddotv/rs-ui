import ConfigService from "./ConfigService";

class AuthService extends ConfigService {
  updateJwtToken = (jwt) => {
    this.setToken(jwt);
  };

  signOut = () => {
    this.setToken(null);
  };

  getMe = async () => {
    const res = await this.userAxiosInstance.get(`/user/me`);
    return res.data;
  };
}

export default new AuthService();
