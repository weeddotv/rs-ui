import axios from "axios";

class BlockChainService {
  getNFTByPackID = async (packID) => {
    const res = await axios({
      method: "post",
      url: `/v1/chain/get_table_rows`,
      baseURL: process.env.NEXT_PUBLIC_BLOCK_CHAIN,
      data: JSON.stringify({
        code: "atomicpacksx",
        table: "packrolls",
        scope: packID,
        json: true,
      }),
    });
    return res?.data;
  };

  checkUnPack = async (assetId) => {
    const res = await axios({
      method: "post",
      url: `/v1/chain/get_table_rows`,
      baseURL: process.env.NEXT_PUBLIC_BLOCK_CHAIN,
      data: JSON.stringify({
        code: "atomicpacksx",
        table: "unboxassets",
        scope: assetId,
        json: true,
      }),
    });
    return res?.data;
  };

}

export default new BlockChainService();
