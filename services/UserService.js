import ConfigService from "./ConfigService";
import axios from "axios";

class UserService extends ConfigService {
  updateTokenId = (token) => {
    if (token) {
      this.userAxiosInstance.defaults.headers.common[
        "Authorization"
      ] = `bearer ${token}`;
    } else {
      this.userAxiosInstance.defaults.headers.common[
        "Authorization"
      ] = `bearer ${null}`;
    }
  };

  getCurrentSale = async () => {
    const res = await this.userAxiosInstance.get("/sale/currentSale");
    return res.data;
  };
  getRankList = async (size = 20) => {
    const res = await this.userAxiosInstance.post("/user/rank_list", {
      from: 0,
      size: size,
    });
    return res.data;
  };

  getArchive = async (size = 50) => {
    const res = await this.userAxiosInstance.post("/sale/get_archive", {
      from: 0,
      size: size,
    });
    return res.data;
  };

  getProductsSale = async (id, size = 50) => {
    const res = await this.userAxiosInstance.post(`/product/sale`, {
      saleId: id,
      from: 0,
      size: size,
    });
    return res.data;
  };

  getProductByID = async (id) => {
    const res = await this.userAxiosInstance.get(`/product/get/${id}`);
    return res.data;
  };

  onSubscription = async (email) => {
    const res = await this.userAxiosInstance.post(`/subscription`, {
      email: email,
    });
    return res.data;
  };

  onRedeem = async (values) => {
    const res = await this.userAxiosInstance.post(`/asset/redeem`, {
      ...values,
    });
    return res.data;
  };

  transfer = async (values) => {
    const res = await this.userAxiosInstance.post(`/asset/transfer`, {
      ...values,
    });
    return res.data;
  };

  getStashByUserId = async (userId, from = 0, size = 50) => {
    const res = await this.userAxiosInstance.post(`/asset/user`, {
      userId: userId,
      from: from,
      size: size,
    });
    return res.data;
  };

  unPack = async (assetId) => {
    const res = await this.userAxiosInstance.post(`/asset/unpack`, {
      assetId: assetId,
    });
    return res.data;
  };

  getUnPackRes = async (assetId) => {
    const res = await this.userAxiosInstance.post(`/asset/get_unpack_result`, {
      assetId: assetId,
    });
    return res.data;
  };

  buy = async (productId) => {
    const res = await this.userAxiosInstance.post(`/product/buy`, {
      productId: productId,
      quantity: 1,
      successUrl: `${
        window.location.origin === "http://localhost:3000"
          ? "https://rs-ui-weeddotv.vercel.app"
          : window.location.origin
      }/payment/success?id=${productId}`,
      cancelUrl: `${
        window.location.origin === "http://localhost:3000"
          ? "https://rs-ui-weeddotv.vercel.app"
          : window.location.origin
      }/payment/cancel?id=${productId}`,
    });

    return res.data;
  };

  getUserInfo = async (userId) => {
    const res = await this.userAxiosInstance.get(`/user/info/${userId}`);
    return res.data;
  };

  updateMe = async (data) => {
    const res = await this.userAxiosInstance.post(`/user/update`, data);
    return res.data;
  };
}

export default new UserService();
