import axios from "axios";

class AtomicAssetService {

  getTemplateByCollectionName = async (collectionName) => {
    const res = await axios({
      method: "get",
      url: `/v1/templates?collection_name=${collectionName}`,
      baseURL: process.env.NEXT_PUBLIC_ATOMIC_ASSET,
    });

    return res?.data?.data;
  };

  getCollectionByName = async (collectioName) => {
    const res = await axios({
      method: "get",
      url: `/v1/collections/${collectioName}`,
      baseURL: process.env.NEXT_PUBLIC_ATOMIC_ASSET,
    });
    return res?.data;
  };

  getCollectionByIds = async (ids) => {
    const res = await axios({
      method: "get",
      url: `/v1/collections/?ids=${ids}`,
      baseURL: process.env.NEXT_PUBLIC_ATOMIC_ASSET,
    });
    return res?.data;
  };

  getTemplateByIds = async (ids) => {
    const res = await axios({
      method: "get",
      url: `/v1/templates?ids=${ids}`,
      baseURL: process.env.NEXT_PUBLIC_ATOMIC_ASSET,
    });
    return res?.data?.data;
  };

  getAssetByIds = async (ids) => {
    const res = await axios({
      method: "get",
      url: `/v1/assets?ids=${ids}`,
      baseURL: process.env.NEXT_PUBLIC_ATOMIC_ASSET,
    });
    return res?.data?.data;
  };


}

export default new AtomicAssetService();
